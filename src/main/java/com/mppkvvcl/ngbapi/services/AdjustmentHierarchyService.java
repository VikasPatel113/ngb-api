package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentHierarchyInterface;
import com.mppkvvcl.ngbdao.daos.AdjustmentHierarchyDAO;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ANKIT on 19-08-2017.
 */
@Service
public class AdjustmentHierarchyService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private Logger logger  = GlobalResources.getLogger(AdjustmentHierarchyService.class);

    /**
     * Asking spring to inject dependency on AdjustmentHierarchyRepository.
     * So that we can use it to perform various operation through repo.
     */
    @Autowired
    private AdjustmentHierarchyDAO adjustmentHierarchyDAO;

    public List<AdjustmentHierarchyInterface> getByUserPriorityAndAdjustmentPriority(int userPriority, int adjustmentPriority){
        String methodName = "getByUserPriorityAndAdjustmentPriority() : ";
        logger.info(methodName+" called");
        return adjustmentHierarchyDAO.getByPriorityBetweenUserPriorityAndAdjustmentPriority(userPriority,adjustmentPriority);
    }
}
