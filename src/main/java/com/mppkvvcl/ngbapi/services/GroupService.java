package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.GroupInterface;
import com.mppkvvcl.ngbdao.daos.GroupDAO;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.UserDetailInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PREETESH on 9/1/2017.
 */
@Service
public class GroupService {

    @Autowired
    private GroupDAO groupDAO;

    @Autowired
    private UserDetailService userDetailService;

    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(GroupService.class);

    public List<GroupInterface> getByLocationCode(String locationCode){
        String methodName = " getByLocationCode() : ";
        List<GroupInterface> groups = null;
        logger.info(methodName + "Started fetching groups for location code  ");
        if(locationCode != null){
            logger.info(methodName + " Calling GroupRepository  ");
            groups = groupDAO.getByLocationCode(locationCode);
            if (groups != null && groups.size()>0){
                logger.info(methodName + "Successfully fetched groups");
            }else {
                logger.error(methodName + "no groups found");
            }
        }else{
            logger.error(methodName + " Received locationCode string is null ");
        }
        return groups;
    }

    /**
     * Created By : Preetesh Date: 1/Nov/2017
     * @param groupToCreate
     * @param errorMessage
     * @return
     * @throws Exception
     */
    public GroupInterface create(GroupInterface groupToCreate, ErrorMessage errorMessage) {
        String methodName = " create() : ";
        logger.info(methodName + "called");
        GroupInterface createdGroup = null;
        if(groupToCreate == null) {
            logger.error(methodName + "Input param group found null");
            errorMessage.setErrorMessage("Input param group found null");
            return null;
        }
        String locationCodeInGroup = groupToCreate.getLocationCode();
        String groupNo =  groupToCreate.getGroupNo();

        if(locationCodeInGroup == null || groupNo == null){
            logger.error(methodName + "Group Object's Parameters found null");
            errorMessage.setErrorMessage("Group Object's Parameters found null");
            return null;
        }

        UserDetailInterface userDetail = userDetailService.getLoggedInUserDetails();
        if(!userDetail.getLocationCode().equals(locationCodeInGroup)){
            logger.error(methodName + "loggedInUser location-code and new group's location-code are not matching");
            errorMessage.setErrorMessage("loggedInUser location-code and new group's location-code are not matching");
            return null;
        }

/**
 *       GroupInterface existingGroup = groupDAO.getByGroupNo(groupNo);
 *        if(existingGroup != null){
 *            logger.error(methodName + "Group already present with groupNo "+groupNo);
 *            errorMessage.setErrorMessage("Group already present with groupNo "+groupNo);
 *            return null;
 *        }
 **/
        createdGroup = groupDAO.add(groupToCreate);
        if (createdGroup == null){
            logger.error(methodName + "Unable to create group");
            errorMessage.setErrorMessage("Unable to create group");
            return null;
        }
        return createdGroup;
    }

    public GroupInterface getByGroupNo(String groupNo) {
        String methodName = "getByGroupNo() : ";
        GroupInterface group = null;
        logger.info(methodName + "called");
        if(groupNo != null){
            group = groupDAO.getByGroupNo(groupNo);
        }else{
            logger.error(methodName + "request has group no null.Please check.");
        }
        return group;
    }
}
