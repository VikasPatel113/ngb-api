package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.factories.ConsumerConnectionInformationHistoryFactory;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.utility.NGBAPIUtility;
import com.mppkvvcl.ngbdao.daos.ConsumerConnectionInformationDAO;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationHistoryInterface;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationInterface;
import com.mppkvvcl.ngbinterface.interfaces.ScheduleInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by SUMIT on 14-06-2017.
 */
@Service
public class ConsumerConnectionInformationService {


    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerConnectionInformationService.class);
    /**
     * Asking Spring to inject ConsumerConnectionInformationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer_connection_information_history table
     * at the backend Database
     */
    @Autowired
    private ConsumerConnectionInformationDAO consumerConnectionInformationDAO;

    @Autowired
    private GlobalResources globalResources;

    /**
     * Asking spring to inject EntityManager for underlying hibernate
     * for creating sessions and transactions;
     */
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ConsumerConnectionInformationHistoryService consumerConnectionInformationHistoryService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private BillService billService;

    /**
     * This method takes ConsumerConnectionInformation object and insert it into the backend database.
     * Return insertedConsumerConnectionInformation if insertion successful else return null.<br><br>
     * param consumerConnectionInformation<br>
     * return insertedConsumerConnectionInformation
     */
    public ConsumerConnectionInformationInterface insert(ConsumerConnectionInformationInterface consumerConnectionInformation){
        String methodName = "insert: ";
        ConsumerConnectionInformationInterface insertedConsumerConnectionInformation = null ;
        logger.info(methodName + "insert consumer connection information started");
        if(consumerConnectionInformation != null){
            insertedConsumerConnectionInformation = consumerConnectionInformationDAO.add(consumerConnectionInformation);
            if(insertedConsumerConnectionInformation != null){
                logger.info(methodName + "successfully inserted ConsumerConnectionInformation row");
            }else{
                logger.error(methodName + "Insertion of ConsumerConnectionInformation failed");
            }
        }else{
            logger.info(methodName + "ConsumerConnectionInformation to insert is null ");
        }
        return insertedConsumerConnectionInformation;
    }

    /**
     * This method takes consumerNo from user and fetch a consumerConnectionInformation object against it.
     * Return consumerConnectionInformation if successful else return null.<br><br>
     * param consumerNo<br>
     * return consumerConnectionInformation
     */
    public ConsumerConnectionInformationInterface getByConsumerNo(String consumerNo) {
        String methodName = "getByConsumerNo() : ";
        logger.info(methodName + " service method called for fetching Consumer Connection Details by Consumer No: " + consumerNo);
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        if (consumerNo != null) {
            consumerNo=consumerNo.trim();
            consumerConnectionInformation = consumerConnectionInformationDAO.getByConsumerNo(consumerNo);
            if (consumerConnectionInformation != null) {
                logger.info(methodName + " fetched consumer connection info:" + consumerNo);
            } else {
                logger.error(methodName + " nothing retrieved for consumer number:" + consumerNo);
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
        }
        return consumerConnectionInformation;
    }

    @Transactional(rollbackFor = Exception.class)
    public ConsumerConnectionInformationInterface update(final String consumerNo,Map<String,String> consumerConnectionInformationMap) throws Exception {
        final String methodName = "update() : ";
        logger.info(methodName + "called for " + consumerNo);
        if (consumerNo == null || consumerConnectionInformationMap == null) {
            logger.error(methodName + "Inputs are null");
            return null;
        }

        ConsumerConnectionInformationInterface consumerConnectionInformationInterface = getByConsumerNo(consumerNo);
        if (consumerConnectionInformationInterface == null) {
            logger.error(methodName + "No ConsumerInformation Found");
            return null;
        }

        detachConsumerConnectionInformation(consumerConnectionInformationInterface);

        List<ConsumerConnectionInformationHistoryInterface> consumerConnectionInformationHistoryInterfaces = new ArrayList<>();
        for (Map.Entry<String, String> entry : consumerConnectionInformationMap.entrySet()) {
            if (entry == null) continue;

            String key = entry.getKey();
            String value = entry.getValue();

            Method setterMethod = NGBAPIUtility.getSetterForField(key, consumerConnectionInformationInterface);
            Method getterMethod = NGBAPIUtility.getGetterForField(key, consumerConnectionInformationInterface);

            if (setterMethod == null || getterMethod == null) {
                logger.error(methodName + "Setter/Getter Method Not found for key " + key + " continuing");
                continue;
            }

            Field field = NGBAPIUtility.getFieldByName(key, consumerConnectionInformationInterface);
            if (field == null) {
                logger.error(methodName + "Field Not found for key " + key + " continuing");
                continue;
            }

            Column columnAnnotation = NGBAPIUtility.getColumnAnnotationByNameValue(field);
            if (columnAnnotation == null) {
                logger.error(methodName + "ColumnAnnotation Not found for key " + key + " continuing");
                continue;
            }

            logger.info(methodName + "calling " + getterMethod.getName() + " to get old value ");
            Object getResult = getterMethod.invoke(consumerConnectionInformationInterface, null);
            String olderValue = "";
            if (getResult != null) {
                olderValue = getResult.toString();
            }

            String billMonthToInsert = null;
            BillInterface bill = billService.getLatestBillByConsumerNo(consumerNo);
            if (bill != null && bill.getBillMonth() != null) {
                billMonthToInsert = bill.getBillMonth();
            } else {
                ScheduleInterface scheduleInterface = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
                if (scheduleInterface == null || scheduleInterface.getBillMonth() == null) {
                    logger.error(methodName + "no completed scheduleInterface found");
                    throw new Exception("no completed scheduleInterface found");
                }
                billMonthToInsert = scheduleInterface.getBillMonth();
            }

            logger.info(methodName + "Old Value is " + olderValue + " new value is " + value);
            ConsumerConnectionInformationHistoryInterface consumerConnectionInformationHistoryInterface = ConsumerConnectionInformationHistoryFactory.build();
            consumerConnectionInformationHistoryInterface.setConsumerNo(consumerNo);
            consumerConnectionInformationHistoryInterface.setPropertyName(columnAnnotation.name());
            consumerConnectionInformationHistoryInterface.setPropertyValue(olderValue);
            consumerConnectionInformationHistoryInterface.setEndDate(GlobalResources.getCurrentDate());
            consumerConnectionInformationHistoryInterface.setEndBillMonth(billMonthToInsert);

            //Adding to list to insert after every column update
            consumerConnectionInformationHistoryInterfaces.add(consumerConnectionInformationHistoryInterface);
            logger.info(methodName + "calling " + setterMethod.getName() + " with value " + value);
            Object argumentValue = NGBAPIUtility.getParameterValueBySetterMethodAndValue(setterMethod,value);
            logger.info(methodName + "calling invoke with argument value " + argumentValue);
            setterMethod.invoke(consumerConnectionInformationInterface, argumentValue);
        }
        if (consumerConnectionInformationMap.size() != consumerConnectionInformationHistoryInterfaces.size()) {
            logger.error(methodName + "Map Size and HistoryInformation does not match. Aborting and rolling back");
            throw new Exception("Map Size and HistoryInformation does not match. Aborting and rolling back");
        }

        List<ConsumerConnectionInformationHistoryInterface> insertedConsumerConnectionInformationHistoryInterfaces = consumerConnectionInformationHistoryService.insert(consumerConnectionInformationHistoryInterfaces);
        if (insertedConsumerConnectionInformationHistoryInterfaces == null || insertedConsumerConnectionInformationHistoryInterfaces.size() != consumerConnectionInformationHistoryInterfaces.size()) {
            logger.error(methodName + "Inserted HistoryInformations not equal to passed HistoryInformations.Aborting and rolling back");
            throw new Exception("Map Size and HistoryInformation does not match. Aborting and rolling back");
        }

        ConsumerConnectionInformationInterface updatedConsumerConnectionInformationInterface = update(consumerConnectionInformationInterface);
        if (updatedConsumerConnectionInformationInterface == null) {
            logger.error(methodName + "Unable to Inserted ConsumerInformation.Aborting and rolling back");
            throw new Exception("Unable to Inserted ConsumerInformation.Aborting and rolling back");
        }
        return updatedConsumerConnectionInformationInterface;
    }

    public ConsumerConnectionInformationInterface update(ConsumerConnectionInformationInterface consumerConnectionInformationInterface) {
        String methodName = "update() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationInterface updatedConsumerConnectionInformationInterface = null;
        if (consumerConnectionInformationInterface != null) {
            setUpdateAuditDetails(consumerConnectionInformationInterface);
            updatedConsumerConnectionInformationInterface = consumerConnectionInformationDAO.update(consumerConnectionInformationInterface);
        }
        return updatedConsumerConnectionInformationInterface;
    }

    private void detachConsumerConnectionInformation(ConsumerConnectionInformationInterface consumerConnectionInformationInterface){
        final String methodName = "detachConsumerConnectionInformation() : ";
        logger.info(methodName + "called");
        if(consumerConnectionInformationInterface != null){
            entityManager.detach(consumerConnectionInformationInterface);
        }
    }

    private void setAuditDetails(ConsumerConnectionInformationInterface consumerConnectionInformationInterface) {
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (consumerConnectionInformationInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            consumerConnectionInformationInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            consumerConnectionInformationInterface.setCreatedOn(GlobalResources.getCurrentDate());
            consumerConnectionInformationInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            consumerConnectionInformationInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }

    public void setUpdateAuditDetails(ConsumerConnectionInformationInterface consumerConnectionInformationInterface) {
        final String methodName = "setUpdateAuditDetails() : ";
        logger.info(methodName + "called");
        if (consumerConnectionInformationInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            consumerConnectionInformationInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            consumerConnectionInformationInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }
}
