package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.LocalHolidayInterface;
import com.mppkvvcl.ngbdao.daos.LocalHolidayDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by PREETESH on 9/11/2017.
 */
@Service
public class LocalHolidayService {

    private static final Logger logger = GlobalResources.getLogger(HolidayService.class);

    @Autowired
    private LocalHolidayDAO localHolidayDAO;

    public boolean checkLocalHoliday(Date date, String locationCode) {
        String methodName = " checkLocalHoliday(): ";
        LocalHolidayInterface localHoliday = null;
        boolean holidayCheck = false;
        logger.info(methodName + "got request to check holiday for day "+date);
        localHoliday = localHolidayDAO.getByLocationCodeAndDate(locationCode,date);
        if (localHoliday != null ) {
            holidayCheck = true;
            logger.info(methodName + "on date "+date+", we have found a local holiday " + localHoliday);
        } else {
            logger.info(methodName + " this is not a holiday on date: " + date);
        }
        return holidayCheck;
    }
}
