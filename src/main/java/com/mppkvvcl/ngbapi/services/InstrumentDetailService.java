package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.CustomInstrumentDetail;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentDetailInterface;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentDishonourInterface;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentPaymentMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import com.mppkvvcl.ngbdao.daos.InstrumentDetailDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 14-07-2017.
 */
@Service
public class InstrumentDetailService {
    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(InstrumentDetailService.class);

    @Autowired
    private InstrumentDetailDAO instrumentDetailDAO;

    @Autowired
    private InstrumentPaymentMappingService instrumentPaymentMappingService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private InstrumentDishonourService instrumentDishonourService;

    @Autowired
    private CashWindowStatusService cashWindowStatusService;

    /**
     * This getByDishonoured method takes dishonoured and return list against it.
     * Return instrumentDetailList if successful else return null.<br><br>
     * param dishonoured<br>
     * return List of InstrumentDetail<br>
     */

    public List<InstrumentDetailInterface> getByDishonoured(boolean dishonoured){
        String methodName = "getByDishonoured () : ";
        List<InstrumentDetailInterface> instrumentDetailList = null;
        logger.info(methodName + "Got request to view InstrumentDetail against dishonoured : " + dishonoured);
        logger.info(methodName + "Calling getByDishonoured method to get InstrumentDetails against dishonoured : " + dishonoured);
        instrumentDetailList = instrumentDetailDAO.getByDishonoured(dishonoured);
        if(instrumentDetailList != null){
            if(instrumentDetailList.size() > 0){
                logger.info(methodName + "InstrumentDetails list received against dishonoured : " + dishonoured + "with rows : " + instrumentDetailList.size());
            }else{
                logger.error(methodName + "No content found in list");
            }
        }else{
            logger.error(methodName + "List not found against param dishonoured : " + dishonoured);
        }
        return instrumentDetailList;
    }

    /**
     *
     * @param payMode
     * @param bankName
     * @param instrumentNo
     * @return
     * @throws Exception
     */
    public CustomInstrumentDetail getByPayModeAndBankNameAndInstrumentNo(String payMode , String bankName , String instrumentNo) throws  Exception{
        String methodName = "getByPayModeAndBankNameAndInstrumentNo() :";
        logger.info( methodName + "Got request to Search instrument Details by Given Pay Mode "+payMode+"Bank Name "+bankName+" Instrument No"+instrumentNo);
        List<InstrumentDetailInterface> instrumentDetails = new ArrayList<>();
        List<PaymentInterface> payments = null;
        List<InstrumentPaymentMappingInterface> instrumentPaymentMappings = null;
        CustomInstrumentDetail customInstrumentDetail = null;
        if(payMode != null && bankName != null && instrumentNo !=  null){
            logger.info( methodName + "Calling InstrumentDetailRepository to Find by Bank Name and instrument No");
            InstrumentDetailInterface instrumentDetail = instrumentDetailDAO.getByPayModeAndBankNameAndInstrumentNo(payMode,bankName,instrumentNo);
            if(instrumentDetail != null){
                instrumentDetails.add(instrumentDetail);
                long instrumentDetailId = instrumentDetail.getId();
                instrumentPaymentMappings = instrumentPaymentMappingService.getByInstrumentDetailId(instrumentDetailId);
                if(instrumentPaymentMappings != null && instrumentPaymentMappings.size() > 0){
                    payments = new ArrayList<>();
                    logger.info( methodName + "Fetched InstrumentPayment Mapping"+instrumentPaymentMappings);
                    for (InstrumentPaymentMappingInterface instrumentPaymentMapping : instrumentPaymentMappings){
                        long paymentId = instrumentPaymentMapping.getPaymentId();
                        // Call Service Payment
                        PaymentInterface payment = paymentService.getOne(paymentId);
                        if(payment != null){
                            logger.info( methodName + "Fetched  Payment for Payment Id : "+paymentId);
                            payments.add(payment);
                        }else {
                            logger.error( methodName +"No payment found for Payment Id "+paymentId);
                        }
                    }
                    logger.info( methodName + "Setting variables in custom Instrument Details :"+instrumentDetail+" Instrument Payment Mapping :"+instrumentPaymentMappings);
                    customInstrumentDetail = new CustomInstrumentDetail();
                    customInstrumentDetail.setInstrumentDetails(instrumentDetails);
                    customInstrumentDetail.setInstrumentPaymentMappings(instrumentPaymentMappings);
                    customInstrumentDetail.setPayments(payments);
                }else {
                    logger.error( methodName + "Unable to fetch InstrumentPayment Mapping");
                    throw new Exception( "Unable to fetch InstrumentPayment Mapping");
                }
            }else {
                logger.error( methodName + "Given PayMode And Bank Name And Instrument No combination doesn't exist in InstrumentDetails");
                throw new Exception( "Given PayMode And Bank Name And Instrument No combination doesn't exist in InstrumentDetails");
            }
        }else {
            logger.error( methodName + "Given PayMode OR Bank Name OR Instrument No is null");
            throw new Exception( "Given PayMode OR Bank Name OR Instrument No is null");
        }
        return customInstrumentDetail;
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean deleteById(long instrumentDetailId) throws Exception {
        String methodName = "deleteById() : ";
        logger.info(methodName + "called");
        final InstrumentDetailInterface instrumentInDb = instrumentDetailDAO.getById(instrumentDetailId);
        logger.info("Instrument Mapping Found " + instrumentInDb);
        if (instrumentInDb == null) {
            final String message = "instrument not found";
            logger.error(methodName + message);
            throw new Exception(message);
        }
        instrumentDetailDAO.deleteById(instrumentDetailId);
        return true;
    }

    /**
     * Inserting the Instrument Details
     *
     * @param instrumentDetail
     * @return
     */
    public InstrumentDetailInterface insert(InstrumentDetailInterface instrumentDetail) {
        String methodName = "insert() : ";
        InstrumentDetailInterface insertedInstrumentDetail = null;
        logger.info(methodName + "called");
        if (instrumentDetail != null) {
            setAuditDetails(instrumentDetail);
            insertedInstrumentDetail = instrumentDetailDAO.add(instrumentDetail);
        }
        return insertedInstrumentDetail;
    }

    /**
     * Updating the Instrument Details
     *
     * @param instrumentDetail
     * @return
     */
    public InstrumentDetailInterface update(InstrumentDetailInterface instrumentDetail) {
        String methodName = "update() : ";
        InstrumentDetailInterface insertedInstrumentDetail = null;
        logger.info(methodName + "called");
        if (instrumentDetail != null) {
            setUpdatedDetails(instrumentDetail);
            insertedInstrumentDetail = instrumentDetailDAO.update(instrumentDetail);
        }
        return insertedInstrumentDetail;
    }

    private void setUpdatedDetails(InstrumentDetailInterface instrumentDetail) {
        String methodName = "setUpdatedDetails() : ";
        logger.info(methodName + "called");
        if (instrumentDetail != null) {
            instrumentDetail.setUpdatedOn(GlobalResources.getCurrentDate());
            instrumentDetail.setUpdatedBy(GlobalResources.getLoggedInUser());
        }
    }

    private void setAuditDetails(InstrumentDetailInterface instrumentDetail) {
        String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (instrumentDetail != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            instrumentDetail.setUpdatedOn(date);
            instrumentDetail.setUpdatedBy(user);
            instrumentDetail.setCreatedOn(date);
            instrumentDetail.setCreatedBy(user);
        }
    }

    public CustomInstrumentDetail getById (long id) throws  Exception{
        String methodName = "getById() :";
        logger.info( methodName + "Got request to Search instrument Details by Given Instrument Id :"+id);
        List<InstrumentDetailInterface> instrumentDetails = new ArrayList<>();
        List<PaymentInterface> payments = null;
        List<InstrumentPaymentMappingInterface> instrumentPaymentMappings = null;
        CustomInstrumentDetail customInstrumentDetail = null;
        if(id > 0){
            logger.info( methodName + "Calling InstrumentDetailRepository to Find by instrument id :"+id);
            InstrumentDetailInterface instrumentDetail = instrumentDetailDAO.getById(id);
            if(instrumentDetail != null){
                instrumentDetails.add(instrumentDetail);
                long instrumentDetailId = instrumentDetail.getId();
                instrumentPaymentMappings = instrumentPaymentMappingService.getByInstrumentDetailId(instrumentDetailId);
                if(instrumentPaymentMappings != null && instrumentPaymentMappings.size() > 0){
                    payments = new ArrayList<PaymentInterface>();
                    logger.info( methodName + "Fetched InstrumentPayment Mapping"+instrumentPaymentMappings);
                    for (InstrumentPaymentMappingInterface instrumentPaymentMapping : instrumentPaymentMappings){
                        long paymentId = instrumentPaymentMapping.getPaymentId();
                        // Call Service Payment
                        PaymentInterface payment = paymentService.getOne(paymentId);
                        if(payment != null){
                            logger.info( methodName + "Fetched  Payment for Payment Id : "+paymentId);
                            payments.add(payment);
                        }else {
                            logger.error( methodName +"No payment found for Payment Id "+paymentId);
                        }
                    }
                    logger.info( methodName + "Setting variables in custom Instrument Details :"+instrumentDetail+" Instrument Payment Mapping :"+instrumentPaymentMappings);
                    customInstrumentDetail = new CustomInstrumentDetail();
                    customInstrumentDetail.setInstrumentDetails(instrumentDetails);
                    customInstrumentDetail.setInstrumentPaymentMappings(instrumentPaymentMappings);
                    customInstrumentDetail.setPayments(payments);
                }else {
                    logger.error( methodName + "Unable to fetch InstrumentPayment Mapping");
                    throw new Exception( "Unable to fetch InstrumentPayment Mapping");
                }
            }else {
                logger.error( methodName + "Given Instrument Id doesn't exist in InstrumentDetails");
                throw new Exception( "Given Instrument Id doesn't exist in InstrumentDetails");
            }
        }else {
            logger.error( methodName + "Given Instrument Id is null");
            throw new Exception( "Given Instrument Id is null");
        }
        return customInstrumentDetail;
    }

    /**
     *
     * @param id
     * @param remark
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public List<InstrumentDishonourInterface> updateById(long id, String remark) throws Exception {
        final String methodName = "updateById() : ";
        logger.info(methodName + "called");
        InstrumentDetailInterface instrumentDetail = null;
        List<InstrumentDishonourInterface> dishonouredInstruments = null;
        logger.info(methodName + "Fetching Instrument Detail");
        instrumentDetail = instrumentDetailDAO.getById(id);
        if (instrumentDetail == null) {
            final String message = "Instrument Id not found";
            logger.error(methodName + message);
            throw new Exception(message);
        }
        if (instrumentDetail.isDishonoured()) {
            final String message = "Instrument already dishonoured";
            logger.error(methodName + message);
            throw new Exception(message);
        }
        instrumentDetail.setDishonoured(true);
        InstrumentDetailInterface insertedInstrumentDetail = update(instrumentDetail);
        if (insertedInstrumentDetail != null) {
            logger.info(methodName + "inserted Instrument Detail Dishonoured Flag as True");
            dishonouredInstruments = instrumentDishonourService.insertDishonour(id, remark);
            if (dishonouredInstruments == null) {
                final String message = "Some error in insertion of Instrument Dishonour";
                logger.error(methodName + message);
                throw new Exception(message);
            }
        }
        return dishonouredInstruments;
    }

    /**
     * Fetch the Consumer Instrument Detail by consumer No and Pay modes
     *
     * @param consumerNo
     * @param payModes
     * @return
     * @throws Exception
     */
    public CustomInstrumentDetail getByConsumerNoAndPayModes(final String consumerNo, final String[] payModes) throws Exception {
        final String methodName = "getByConsumerNoAndPayModes() : ";
        logger.info(methodName + "called with Consumer No: " + consumerNo + " and Pay Modes: " + Arrays.toString(payModes));
        if (consumerNo == null || consumerNo.isEmpty() || payModes == null || payModes.length == 0) {
            final String message = "Given Consumer No is null or No Payment modes available";
            logger.error(methodName + message);
            throw new Exception(message);
        }
        final List<InstrumentDetailInterface> instrumentDetails = new ArrayList<>();
        final List<InstrumentPaymentMappingInterface> instrumentPaymentMappings = new ArrayList<>();

        logger.info(methodName + "Calling PaymentRepository to Find by Consumer No and Payment Modes");
        final List<PaymentInterface> payments = paymentService.getByConsumerNoAndPayModes(consumerNo, payModes);
        if (payments == null || payments.isEmpty()) {
            final String message = "No payments found for Consumer No" + consumerNo + " and Pay Modes: " + Arrays.toString(payModes);
            logger.error(methodName + message);
            throw new Exception(message);
        }
        for (PaymentInterface payment : payments) {
            final InstrumentPaymentMappingInterface paymentMapping = instrumentPaymentMappingService.getByPaymentId(payment.getId());
            if (paymentMapping == null) {
                final String message = "Unable to fetch InstrumentPayment Mapping for Payment " + String.valueOf(payment.getId());
                logger.error(methodName + message);
                throw new Exception(message);
            }
            instrumentPaymentMappings.add(paymentMapping);
            final InstrumentDetailInterface instrumentDetail = instrumentDetailDAO.getById(paymentMapping.getInstrumentDetailId());
            if (instrumentDetail != null) {
                instrumentDetails.add(instrumentDetail);
            }
        }
        final CustomInstrumentDetail customInstrumentDetail = new CustomInstrumentDetail();
        customInstrumentDetail.setInstrumentDetails(instrumentDetails);
        customInstrumentDetail.setInstrumentPaymentMappings(instrumentPaymentMappings);
        customInstrumentDetail.setPayments(payments);
        return customInstrumentDetail;
    }

    /**
     * Returns the Consumer Payments and Instrument details as per the Instrument No
     *
     * @param instrumentNo
     * @return
     * @throws Exception
     */
    public CustomInstrumentDetail getByInstrumentNo(final String instrumentNo) throws Exception {
        final String methodName = "getByConsumerNoAndPayModes() : ";
        logger.info(methodName + "called with Instrument No: " + instrumentNo);
        if (instrumentNo == null || instrumentNo.isEmpty()) {
            final String message = "Given Instrument No is null available";
            logger.error(methodName + message);
            throw new Exception(message);
        }
        final List<PaymentInterface> payments = new ArrayList<>();
        final List<InstrumentPaymentMappingInterface> instrumentPaymentMappings = new ArrayList<>();

        logger.info(methodName + "Calling InstrumentDetailRepository to Find by Instrument No");
        final List<InstrumentDetailInterface> instrumentDetails = instrumentDetailDAO.getByInstrumentNo(instrumentNo);
        if (instrumentDetails == null || instrumentDetails.isEmpty()) {
            final String message = "No Instruments found for Instrument No" + instrumentNo;
            logger.error(methodName + message);
            throw new Exception(message);
        }
        for (InstrumentDetailInterface instrumentDetail : instrumentDetails) {
            final List<InstrumentPaymentMappingInterface> instrumentMappings = instrumentPaymentMappingService.getByInstrumentDetailId(instrumentDetail.getId());
            if (instrumentMappings != null && !instrumentMappings.isEmpty()) {
                instrumentPaymentMappings.addAll(instrumentMappings);
                for (InstrumentPaymentMappingInterface instrumentPaymentMapping : instrumentMappings) {
                    final PaymentInterface payment = paymentService.getOne(instrumentPaymentMapping.getPaymentId());
                    if (payment != null) {
                        payments.add(payment);
                    }
                }
            }
        }
        final CustomInstrumentDetail customInstrumentDetail = new CustomInstrumentDetail();
        customInstrumentDetail.setInstrumentDetails(instrumentDetails);
        customInstrumentDetail.setInstrumentPaymentMappings(instrumentPaymentMappings);
        customInstrumentDetail.setPayments(payments);
        return customInstrumentDetail;
    }


    /**
     * Added By Preetesh date : 30 Dec 2017
     * Retrieving InstrumentDetails and its all associated payments by supplying single paymentId
     * @param paymentId
     * @param errorMessage
     * @return
     */
    public List<InstrumentPaymentMappingInterface> getByPaymentId(long paymentId, ErrorMessage errorMessage) {
        final String methodName = "getByPaymentId() : ";
        logger.info(methodName + "called ");
        CustomInstrumentDetail customInstrumentDetail = null;
        InstrumentPaymentMappingInterface instrumentPaymentMappingInterface = instrumentPaymentMappingService.getByPaymentId(paymentId);
        if(instrumentPaymentMappingInterface == null){
            logger.error( methodName + "Unable to fetch InstrumentPayment Mapping");
            errorMessage.setErrorMessage("InstrumentPayment Mapping not found");
        }
        long instrumentDetailId = instrumentPaymentMappingInterface.getInstrumentDetailId();
        List<InstrumentPaymentMappingInterface> instrumentPaymentMappingInterfaces = instrumentPaymentMappingService.getByInstrumentDetailId(instrumentDetailId);
        if(instrumentPaymentMappingInterfaces == null || instrumentPaymentMappingInterfaces.size() == 0){
            logger.error( methodName + "Unable to fetch InstrumentPayment Mapping");
            errorMessage.setErrorMessage("InstrumentPayment Mapping not found");
        }

        logger.info(methodName+" mappings"+ instrumentPaymentMappingInterfaces );

        return instrumentPaymentMappingInterfaces;
    }

    /**
     * Added By Preetesh date : 30 Dec 2017
     * 1. Check InstrumentDetails and Payments came for deletion.
     * 2. Retrieve Existing Instrument and Its associated payments
     * 3. Check if any payment is posted true , if not , Set Existing Payments as Deleted TRUE
     */
    @Transactional(rollbackFor = Exception.class)
    public CustomInstrumentDetail deleteInstrumentDetailById(long instrumentId)throws Exception {
        String methodName = "deleteInstrumentDetailById() : ";
        logger.info(methodName + "called");

        CustomInstrumentDetail customInstrumentDetailToUpdate = getById(instrumentId);
        if (customInstrumentDetailToUpdate == null) {
            logger.error(methodName + "Instrument and associated payments not found ");
            throw new Exception("Instrument and associated payments not found ");
        }

        InstrumentDetailInterface instrumentDetailInterfaceToUpdate = customInstrumentDetailToUpdate.getInstrumentDetails().get(0);
        List<PaymentInterface> paymentInterfacesToUpdate =  customInstrumentDetailToUpdate.getPayments();

        if(instrumentDetailInterfaceToUpdate == null || paymentInterfacesToUpdate == null || paymentInterfacesToUpdate.size() == 0 ){
            logger.error(methodName + "instrument to delete not found ");
            throw new Exception("instrument to delete not found ");
        }

        String loggedInUser = GlobalResources.getLoggedInUser();
        Date currentDate = GlobalResources.getCurrentDate();
        /**
         * Verify Payment window
         */
        PaymentInterface paymentInterface = paymentInterfacesToUpdate.get(0);
        Date paymentDate = paymentInterface.getPayDate();
        if(!cashWindowStatusService.isWindowOpened(loggedInUser,paymentDate)){
            logger.error(methodName + "Window Date does'nt match with payment punching date ");
            throw new Exception("Window Date does'nt match with payment punching date");
        }

        List<PaymentInterface> deletedPayments = new ArrayList<>();

        PaymentInterface isPostedPayment = paymentInterfacesToUpdate.stream().filter(p -> {
            return p.isPosted();
        }).findFirst().orElse(null);

        if(isPostedPayment != null){
            logger.error(methodName + " Payment has participated in Billing ");
            throw new Exception("Payment has participated in Billing");
        }

        for(PaymentInterface paymentToCheck : paymentInterfacesToUpdate ){
            paymentToCheck.setDeleted(PaymentInterface.DELETED_TRUE);
            paymentToCheck.setUpdatedOn(currentDate);
            paymentToCheck.setUpdatedBy(loggedInUser);
            PaymentInterface updatedPayment = paymentService.update(paymentToCheck);
            deletedPayments.add(updatedPayment);
        }

        instrumentDetailInterfaceToUpdate.setDeleted(true);
        instrumentDetailInterfaceToUpdate.setUpdatedBy(loggedInUser);
        instrumentDetailInterfaceToUpdate.setUpdatedOn(currentDate);
        InstrumentDetailInterface updatedInstrumentDetailInterface = instrumentDetailDAO.update(instrumentDetailInterfaceToUpdate);
        if (updatedInstrumentDetailInterface == null) {
            logger.error(methodName + "Not able to update instrument ");
            throw new Exception("Not able to update instrument ");
        }
        customInstrumentDetailToUpdate.setPayments(deletedPayments);
        return customInstrumentDetailToUpdate;
    }
}