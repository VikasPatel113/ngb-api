package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.CustomCashWindow;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CashWindowStatusInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import com.mppkvvcl.ngbdao.daos.CashWindowStatusDAO;
import com.mppkvvcl.ngbentity.beans.CashWindowStatus;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import com.mppkvvcl.ngbentity.beans.WindowDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 7/25/2017.
 */
@Service
public class CashWindowStatusService {

    /**
     * Requesting spring to inject singleton CashWindowStatusRepository object.
     */
    @Autowired
    private CashWindowStatusDAO cashWindowStatusDAO;

    /**
     * Requesting spring to inject singleton PaymentService object.
     */
    @Autowired
    private PaymentService paymentService;

    @Autowired
    private UserDetailService userDetailService;

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(CashWindowStatusService.class);

    /**
     * This method takes userName and status and fetches a list of cashWindowStatuses from backend database.
     * Return list if successful else return null.<br><br>
     * param userName<br>
     * param status<br>
     * return cashWindowStatuses
     */
    public List<CashWindowStatusInterface> getByUsernameAndStatus(String userName, String status) {
        String methodName = " getByUsernameAndStatus(): ";
        List<CashWindowStatusInterface> cashWindowStatuses = null;
        logger.info(methodName + "fetch cash windows started by username "+userName+" status "+status);
        if (userName != null && status != null) {
            cashWindowStatuses =  cashWindowStatusDAO.getByUsernameAndStatus(userName,status);
            if(cashWindowStatuses != null && cashWindowStatuses.size() > 0){
                logger.info(methodName + "successfully fetched rows");
            }else{
                logger.error(methodName + "no window found for user: "+userName+" with status "+status);
            }
        }
        else{
            logger.error(methodName + "one of the inputs null");
        }
        return cashWindowStatuses;

    }

    /**
     * This method takes cashWindowStatus object
     * param cashWindowStatus<br>
     * return
     */
    public CustomCashWindow getCustomCashWindow(CashWindowStatusInterface cashWindowStatus) {
        String methodName = " getCustomCashWindow(): ";
        CustomCashWindow customCashWindow = null;
        logger.info(methodName + "preparing custom cash window ");
        if (cashWindowStatus != null ) {
            String location = cashWindowStatus.getLocationCode();
            String windowName = cashWindowStatus.getWindowName();
            Date windowDate = cashWindowStatus.getDate();
            long cashAmount = 0;
            long chequeAmount = 0;
            long cashCounter = 0 ;
            long chequeCounter = 0 ;
            if(location != null && windowName != null && windowDate != null) {
                List<PaymentInterface> payments = paymentService.getByLocationAndWindowNameAndDateAndDeleted(location, windowName, windowDate);
                if (payments != null) {
                    int noOfPayments = payments.size();
                    logger.info(methodName + "Got payments nos: " + noOfPayments + " against location : " + location + " windowName : " + windowName + ", date : " + windowDate);
                    if (noOfPayments > 0) {
                        for (PaymentInterface payment : payments) {
                            if(payment != null ){
                                String payMode = payment.getPayMode();
                                long payAmount = payment.getAmount();
                                if(payMode != null && payAmount > 0){
                                    if (payMode.equals(PaymentInterface.PAY_MODE_CASH)) {
                                        cashAmount = cashAmount + payment.getAmount();
                                        cashCounter = cashCounter + 1;
                                    } else {
                                        chequeAmount = chequeAmount + payment.getAmount();
                                        chequeCounter = chequeCounter + 1;
                                    }
                                }
                            }
                        }
                    } else {
                        logger.info(methodName + "zero payments found");
                    }
                } else {
                    logger.info(methodName + "payments found null");
                }
                customCashWindow = new CustomCashWindow();
                customCashWindow.setCashWindowStatus(cashWindowStatus);
                customCashWindow.setCashTotalAmount(cashAmount);
                customCashWindow.setNoOfPaymentsInCash(cashCounter);
                customCashWindow.setChequeTotalAmount(chequeAmount);
                customCashWindow.setNoOfPaymentsInCheque(chequeCounter);
                customCashWindow.setUserDetail(userDetailService.getByUsername(cashWindowStatus.getUsername()));
            }else {
                logger.error(methodName + "location, window name or date is null");
            }
        } else {
            logger.error(methodName + "cashWindow bean passed is null");
        }
        return customCashWindow;
    }

    /**
     * This method takes location and date from user and fetches a list of cashWindowStatuses from backend database.<br><br>
     * param location<br>
     * param date<br>
     * return cashWindowStatuses
     */
    public List<CashWindowStatusInterface> getAssignedWindowsByLocationAndDate(String location, Date date) {
        String methodName = " getAssignedWindowsByLocationAndDate(): ";
        List<CashWindowStatusInterface> cashWindowStatuses = null;
        logger.info(methodName + "fetch cash windows started");
        if (location != null ) {
            cashWindowStatuses =  cashWindowStatusDAO.getByLocationCodeAndDateAndStatus(location,date, CashWindowStatusInterface.STATUS_OPEN);
            if(cashWindowStatuses.size() > 0){
                logger.info(methodName + "successfully fetched open cash windows, nos "+cashWindowStatuses.size());
            }else{
                logger.info(methodName + "no open window found for location");
            }
        }else{
            logger.error(methodName + "location passed is null");
        }
        return cashWindowStatuses;
    }

    /**
     * 1. first check if given-window is already present on given date and location
     * 2. if entry is present in CashWindowStatus table, it means end-user wants to reopen window
     * 3. check for same user, and reopen window; and set the counters of window
     * 4. if window entry is not present in CashWindowStatus table, it can be assigned to user
     * 5. save new CashWindowStatus and initialize counters to Zero
     * 6. return CustomCashWindow
     * @param windowDetail
     * @param freezeDateInString
     * @return
     * @throws RuntimeException
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public CustomCashWindow openCashWindow(WindowDetail windowDetail, String windowAssignedToUser, String freezeDateInString)
            throws RuntimeException, Exception {
        String methodName = " openCashWindow() ";
        CustomCashWindow customCashWindow = null;
        if (windowDetail != null && freezeDateInString != null && windowAssignedToUser != null) {
            Date freezeOnDate = GlobalResources.getDateFromString(freezeDateInString);
            logger.info(methodName + " service method called for freezing window "+windowDetail+" on date "+freezeOnDate);
            String locationCode = windowDetail.getLocationCode();
            String windowName = windowDetail.getName();
            String loggedInUser = GlobalResources.getLoggedInUser();
            Date loggedInTime = GlobalResources.getCurrentDate();
            if(locationCode != null && windowName != null && freezeOnDate != null) {
                List<CashWindowStatusInterface> alreadyHasOpenWindow = getByUsernameAndStatus(windowAssignedToUser,CashWindowStatusInterface.STATUS_OPEN);
                if(alreadyHasOpenWindow != null && alreadyHasOpenWindow.size()==0 ){
                    CashWindowStatusInterface cashWindowStatus = cashWindowStatusDAO.getByLocationCodeAndWindowNameAndDate(locationCode, windowName, freezeOnDate);
                    if(cashWindowStatus != null){
                        String userNameInCashWindow = cashWindowStatus.getUsername();
                        if(windowAssignedToUser.equals(userNameInCashWindow )){
                            cashWindowStatus.setStatus(CashWindowStatusInterface.STATUS_OPEN);
                            cashWindowStatus.setUpdatedBy(loggedInUser);
                            cashWindowStatus.setUpdatedOn(loggedInTime);
                            CashWindowStatusInterface updatedCashWindow = cashWindowStatusDAO.update(cashWindowStatus);
                            if(updatedCashWindow != null){
                                logger.info(methodName + "reopened the window now preparing custom bean "+updatedCashWindow);
                                customCashWindow = getCustomCashWindow(updatedCashWindow);
                            }else{
                                logger.error(methodName + "some error in saving reopen status");
                                throw new Exception("some error in saving reopen status");
                            }
                        }else{
                            logger.error(methodName + "username not matching, cant reopen window");
                            throw new Exception("username not matching, cant reopen window");
                        }
                    }else{
                        logger.info(methodName + "cash window is not assigned to any user, open it");
                        CashWindowStatus cashWindowToOpen = new CashWindowStatus();
                        cashWindowToOpen.setStatus(CashWindowStatusInterface.STATUS_OPEN);
                        cashWindowToOpen.setUsername(windowAssignedToUser);
                        cashWindowToOpen.setDate(freezeOnDate);
                        cashWindowToOpen.setLocationCode(locationCode);
                        cashWindowToOpen.setWindowName(windowName);
                        cashWindowToOpen.setCreatedBy(loggedInUser);
                        cashWindowToOpen.setCreatedOn(loggedInTime);
                        cashWindowToOpen.setUpdatedBy(loggedInUser);
                        cashWindowToOpen.setUpdatedOn(loggedInTime);
                        CashWindowStatusInterface insertedCashWindow = cashWindowStatusDAO.add(cashWindowToOpen);
                        logger.info(methodName + "saved new window as: "+insertedCashWindow);
                        if(insertedCashWindow != null){
                            logger.info(methodName + "opened window now preparing custom bean ");
                            customCashWindow = new CustomCashWindow();
                            customCashWindow.setCashWindowStatus(insertedCashWindow);
                            customCashWindow.setChequeTotalAmount(0);
                            customCashWindow.setCashTotalAmount(0);
                            customCashWindow.setNoOfPaymentsInCheque(0);
                            customCashWindow.setNoOfPaymentsInCash(0);

                        }else{
                            logger.error(methodName + "some error in saving new window");
                            throw new Exception("some error in saving new window");
                        }
                    }
                }else{
                    logger.error(methodName + "user already has an open window");
                    throw new Exception("user already has an open window");
                }

            }else{
                logger.error(methodName + "location code or window name in bean passed is null in method.");
                throw new Exception("Enter location code or window name");
            }
        } else {
            logger.error(methodName + "parameter passed is null in method.");
            throw new Exception("correct date and window detail bean");
        }
        return customCashWindow;
    }

    public String getOpenWindowNameByUserAndDate(String user, Date paymentDate) {
        String methodName = " getOpenWindowNameByUserAndDate(): ";
        List<CashWindowStatusInterface> cashWindowStatuses = null;
        CashWindowStatusInterface cashWindowStatus = null;
        String windowName =  null;
        logger.info(methodName + "fetch cash windows started by username "+user+" status "+paymentDate);
        if (user != null && paymentDate != null) {
            cashWindowStatuses =  cashWindowStatusDAO.getByUsernameAndStatusAndDate(user,CashWindowStatusInterface.STATUS_OPEN,paymentDate);
            if(cashWindowStatuses != null && cashWindowStatuses.size() == 1){
                logger.info(methodName + "successfully fetched open cash window "+cashWindowStatuses);
                cashWindowStatus = cashWindowStatuses.get(0);
                if(cashWindowStatus != null && cashWindowStatus.getWindowName() != null){
                    windowName = cashWindowStatus.getWindowName();
                }else{
                    logger.error(methodName + "no window found for user: "+user+" with status open and date "+paymentDate);
                }

            }else{
                logger.error(methodName + "no window found for user: "+user+" with status open and date "+paymentDate);
            }
        }
        else{
            logger.error(methodName + "one of the inputs null");
        }
        return windowName;
    }

    public List<CustomCashWindow> getAllWindowsDetails() throws RuntimeException, Exception{
        String methodName = " getAllWindowsDetails(): ";
        List<CashWindowStatusInterface> cashWindowStatuses = null;
        List<CustomCashWindow> customCashWindows = null;
        UserDetail userDetail = userDetailService.getLoggedInUserDetails();
        logger.info(methodName + "fetch cash windows started for user "+userDetail);
        if (userDetail != null) {
            String locationCode = userDetail.getLocationCode();
            cashWindowStatuses =  cashWindowStatusDAO.getByLocationCodeAndStatus(locationCode,CashWindowStatusInterface.STATUS_OPEN);
            if(cashWindowStatuses != null && cashWindowStatuses.size() > 0){
                logger.info(methodName + "successfully fetched open cash windows ");
                customCashWindows = new ArrayList<CustomCashWindow>();
                for(CashWindowStatusInterface cashWindowStatus : cashWindowStatuses) {
                    CustomCashWindow customCashWindow = getCustomCashWindow(cashWindowStatus);
                    if(customCashWindow != null){
                        customCashWindows.add(customCashWindow);
                    }else{
                        logger.error(methodName + "some error in creating CashWindowDetails for window "+cashWindowStatus);
                        throw new Exception("some error in creating CashWindowDetails for window "+cashWindowStatus);
                    }
                }
            }else{
                logger.info(methodName + "no open window found for location "+locationCode);
            }
        }
        else{
            logger.error(methodName + "user details not found");
            throw new Exception("user details not found");
        }
        return customCashWindows;
    }

    public List<PaymentInterface> getAllPaymentsOfOpenWindow() throws RuntimeException, Exception{
        String methodName = " getAllPaymentsOfOpenWindow() : ";
        logger.info(methodName + "called");
        CashWindowStatusInterface cashWindowStatus = null;
        List<PaymentInterface> payments = null;
        String loggedInUser = GlobalResources.getLoggedInUser();
        if(loggedInUser != null){
            List<CashWindowStatusInterface> cashWindowStatuses = cashWindowStatusDAO.getByUsernameAndStatus(loggedInUser,CashWindowStatusInterface.STATUS_OPEN);
            if(cashWindowStatuses != null && cashWindowStatuses.size() == 1 ) {
                cashWindowStatus = cashWindowStatuses.get(0);
                if(cashWindowStatus != null) {
                    logger.info(methodName + "found open window for user "+cashWindowStatus);
                    String location = cashWindowStatus.getLocationCode();
                    String windowName = cashWindowStatus.getWindowName();
                    Date windowDate = cashWindowStatus.getDate();
                    if(location != null && windowName != null && windowDate != null) {
                        payments = paymentService.getByLocationAndWindowNameAndDateAndDeleted(location, windowName, windowDate);
                        if (payments != null) {
                            int noOfPayments = payments.size();
                            logger.info(methodName + "Got payments nos: " + noOfPayments + " against location : " + location + " windowName : " + windowName + ", date : " + windowDate);
                            if (noOfPayments > 0) {
                                logger.info(methodName + "payments retrieved successfully, returning from CashWindowStatusService ");
                            } else {
                                logger.info(methodName + "zero payments found");
                            }
                        } else {
                            logger.error(methodName + "payments found null");
                        }
                    }else {
                        logger.error(methodName + "location, window name or date is null");
                        throw new Exception("open cash window not found null");
                    }
                }else{
                    logger.error(methodName + "open cash window found null");
                    throw new Exception("open cash window not found null");
                }
            }else{
                logger.error(methodName + "open cash window not found");
                throw new Exception("open cash window not found");
            }
        } else{
            logger.error(methodName + "user details not found");
            throw new Exception("user details not found");
        }
        return payments;
    }

    public CashWindowStatusInterface closeWindow(String windowName, String freezeDateInString) throws RuntimeException, Exception {
        String methodName = " closeWindow() ";
        boolean closedSuccessfully = false;
        if (windowName == null || freezeDateInString == null) {
            logger.error(methodName + "parameter passed is null in method.");
            throw new Exception("correct date and window detail bean");
        }
            Date freezeOnDate = GlobalResources.getDateFromString(freezeDateInString);
            logger.info(methodName + " service method called for closing window "+windowName+" on date "+freezeOnDate);
       String locationCode = userDetailService.getLoggedInUserDetails().getLocationCode();
       CashWindowStatusInterface cashWindowStatus = cashWindowStatusDAO.getByLocationCodeAndWindowNameAndDate(locationCode,windowName,freezeOnDate);
       if(cashWindowStatus == null){
           logger.error(methodName + "window not found on given date  ");
           throw new Exception("window not found on given date ");
       }
       if(cashWindowStatus.getStatus().equals(CashWindowStatusInterface.STATUS_CLOSE)){
           logger.error(methodName + "window already has a close status  ");
           throw new Exception("window already has a close status ");
       }
        cashWindowStatus.setStatus(CashWindowStatusInterface.STATUS_CLOSE);
        cashWindowStatus.setUpdatedOn(GlobalResources.getCurrentDate());
        cashWindowStatus.setUpdatedBy(GlobalResources.getLoggedInUser());
        CashWindowStatusInterface updatedCashWindowStatus = cashWindowStatusDAO.update(cashWindowStatus);
        if(updatedCashWindowStatus == null){
            logger.error(methodName + "updation failed  ");
            throw new Exception("updation failed  ");
        }
        return updatedCashWindowStatus;
        }

    /**
     * This getUnAssignedUserListByLocationCodeAndDate method fetch Available User List Location Wise  from backend database.<br>
     * Return allAvailableUserOfLocation successful else return null.<br><br>
     * param loggedInUser<br><br>
     * return Likst of Available User<br>
     */
    public List <UserDetail> getUnAssignedUserList() {
        String methodName = " getUnAssignedUserListByLocationCodeAndDate(): ";

        logger.info(methodName + "Fetch available userList ForUser : ");

        UserDetail userDetail = userDetailService.getLoggedInUserDetails();
        List<UserDetail> allUserOfLocation;
        allUserOfLocation = null;
        if (userDetail != null) {
            String location = userDetail.getLocationCode();
            String role=userDetail.getRole();
            allUserOfLocation = userDetailService.getByLocationCodeAndRole(location,role );
            logger.info(methodName + "fetch nos: " + allUserOfLocation.size() + " User Of  location " + location);
            Date todayDate = GlobalResources.getCurrentDate();
            if (allUserOfLocation != null && allUserOfLocation.size() > 0) {
                List<CashWindowStatusInterface> alreadyAssignedUserOfLocation = getAssignedUserByLocationAndDate(location, todayDate);
                logger.info(methodName + "fetch nos: " + alreadyAssignedUserOfLocation.size() + " Assigned User for location " + location);
                if (alreadyAssignedUserOfLocation.size() > 0) {
                    List<UserDetail> unassignedUserOfLocation = allUserOfLocation;
                    alreadyAssignedUserOfLocation.forEach(cashWindowStatus -> {
                        String userName = cashWindowStatus.getUsername();
                        unassignedUserOfLocation.removeIf(userDetailPredicate -> userDetailPredicate.getUsername().equals(userName));

                    });
                } else {
                    logger.error(methodName + "No Open User For   " );
                }
            } else {
                logger.error(methodName + "No Assigned User Found For  ");
            }
        } else {
            logger.error(methodName + "userDetail details not found for userDetail ");
        }

        return allUserOfLocation;
    }


    /**
     * This method takes location and date from user and fetches a list of cashWindowStatuses from backend database.<br><br>
     * param location<br>
     * param date<br>
     * return cashWindowStatuses
     */
    public List<CashWindowStatusInterface> getAssignedUserByLocationAndDate(String location, Date date) {
        String methodName = " getAssignedUserByLocationAndDate(): ";
        List<CashWindowStatusInterface> assignUserStatus=null;
        logger.info(methodName + "fetch cash windows started");
        if (location != null ) {
            assignUserStatus =  cashWindowStatusDAO.getByLocationCodeAndDateAndStatus(location,date, CashWindowStatus.STATUS_OPEN);
            if(assignUserStatus.size() > 0){
                logger.info(methodName + "successfully fetched open cash windows, nos "+assignUserStatus.size());
            }else{
                logger.info(methodName + "no open window found for location");
            }
        }else{
            logger.error(methodName + "location passed is null");
        }
        return assignUserStatus;
    }

    public boolean isWindowOpened(String loggedInUser, Date paymentDate) {
        String methodName = " isWindowOpened(): ";
        List<CashWindowStatusInterface> cashWindowStatuses =  getByUsernameAndStatus(loggedInUser, CashWindowStatusInterface.STATUS_OPEN);
        if (cashWindowStatuses == null || cashWindowStatuses.size() != 1) {
            return false;
        }
        Date windowDate = cashWindowStatuses.get(0).getDate();
        if (windowDate.equals(paymentDate)) {
           return true;
        }
        return false;
    }
}

