package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbinterface.interfaces.DivisionInterface;
import com.mppkvvcl.ngbdao.daos.DivisionDAO;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Rupali on 17-07-2017.
 */
@Service
public class DivisionService {

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(DivisionService.class);

    /**
     * Requesting spring to get singelton DivisionRepository object.
     */
    @Autowired
    private DivisionDAO divisionDAO;

    /**
     * This getAll method show list of all division.
     * Return list of division if successful else return null.<br><br>
     * return List of Division
     */
    public List<? extends DivisionInterface> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName + "called");
        List<? extends DivisionInterface> divisions = null;
        divisions = divisionDAO.getAll();
        return divisions;

    }

    public List<DivisionInterface> getByCircleId(long circleId) {
        final String methodName = "getByCircleId() : ";
        logger.info(methodName + "called");
        return divisionDAO.getByCircleId(circleId);
    }
}
