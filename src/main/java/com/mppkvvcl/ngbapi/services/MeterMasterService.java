package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterMasterInterface;
import com.mppkvvcl.ngbdao.daos.MeterMasterDAO;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SUMIT on 20-06-2017.
 */
@Service
public class MeterMasterService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(MeterMasterService.class);

    @Autowired
    private GlobalResources globalResources;

    /**
     * Asking Spring to inject MeterMasterRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on MeterMaster table
     * at the backend Database
     */
    @Autowired
    private MeterMasterDAO meterMasterDAO;

    /**
     *
     * param meterMaster
     * return
     */
    public MeterMasterInterface insert(MeterMasterInterface meterMaster){
        String methodName = " insert : ";
        MeterMasterInterface insertedMeterMaster = null;
        logger.info(methodName + "called");
        if(meterMaster != null){
            String meterMake = meterMaster.getMake();
            String serialNo = meterMaster.getSerialNo();
            String code = meterMaster.getCode();
            if(meterMake != null && serialNo != null && code != null){
                String identifier = makeIdentifier(meterMake,serialNo);
                if(identifier != null){
                    setAuditDetails(meterMaster);
                    meterMaster.setIdentifier(identifier);
                    insertedMeterMaster = meterMasterDAO.add(meterMaster);
                }
            }
        }
        return insertedMeterMaster;
    }

    /**
     *
     * param make
     * param serialNo
     * return
     */
    public MeterMasterInterface getMeterMasterByMakeAndSerialNo(String make, String serialNo) {
        String methodName = " getMeterMasterByMakeAndSerialNo: ";
        MeterMasterInterface retrievedMeterMaster = null;
        logger.info(methodName + "fetch meter master started");
        if (serialNo != null && make != null) {
            String identifier = make.trim()+serialNo.trim();
            System.out.println(identifier);
            retrievedMeterMaster =  meterMasterDAO.getByIdentifier(identifier);
        }
        return retrievedMeterMaster;
    }

    /**
     *
     * param identifier
     * return
     */

    public MeterMasterInterface getByIdentifier(String identifier) {
        String methodName = "getMeterMasterIdentifier() : ";
        MeterMasterInterface retrievedMeterMaster = null;
        logger.info(methodName + "fetch meter master started");
        if (identifier != null) {
            retrievedMeterMaster =  meterMasterDAO.getByIdentifier(identifier);
        }
        return retrievedMeterMaster;
    }

    /**
     *
     * param make
     * param serialNo
     * return
     */
    private String makeIdentifier(String make,String serialNo){
        if(make != null && serialNo != null){
            return make.trim() + serialNo.trim();
        }
        return null;
    }

    /**
     * using this method until meter type service and repository is made
     * param description
     * return
     */
    private String makeCodeFromDescrption(String description){
        String code = null;
        if(description != null){
            description = description.trim().toLowerCase();
            if(description.equals("whole current single phase meter")){
                code = "WCS";
            }else if(description.equals("whole current three phase meter")){
                code = "WCT";
            }else if(description.equals("current transformer meter")){
                code = "CTT";
            }
        }
        return code;
    }

    private void setAuditDetails(MeterMasterInterface meterMasterInterface){
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if(meterMasterInterface != null){
            String user = GlobalResources.getLoggedInUser();
            meterMasterInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            meterMasterInterface.setCreatedOn(GlobalResources.getCurrentDate());
        }
    }
}
