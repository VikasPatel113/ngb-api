package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.GMCDAO;
import com.mppkvvcl.ngbinterface.interfaces.GMCInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ANSHIKA on 27-07-2017.
 */
@Service
public class GMCService {

    /**
     * Requesting spring to get singleton GMCRepository object.
     */
    @Autowired
    private GMCDAO gmcdao;

    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(GMCService.class);

    public GMCInterface getBySubcategoryCodeAndTariffId(long subCategoryCode ,long tariffId){
        String methodName = "getBySubcategoryCodeAndTariffId() : ";
        logger.info(methodName + "called "+subCategoryCode + "tariff Id "+tariffId);
        GMCInterface gmcInterface = gmcdao.getBySubcategoryCodeAndTariffId(subCategoryCode ,tariffId);
        return gmcInterface;
    }
}
