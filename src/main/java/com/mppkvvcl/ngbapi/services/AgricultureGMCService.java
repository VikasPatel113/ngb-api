package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.AgricultureGMCDAO;
import com.mppkvvcl.ngbinterface.interfaces.AgricultureGMCInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgricultureGMCService {

    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(TariffDetailService.class);

    @Autowired
    private  AgricultureGMCDAO agricultureGMCDAO;

    public List<AgricultureGMCInterface> getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode) {
        final String methodName = "getByTariffIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        List<AgricultureGMCInterface> agricultureGMCInterfaces = agricultureGMCDAO.getByTariffIdAndSubcategoryCode(tariffId,subcategoryCode);
        return agricultureGMCInterfaces;
    }
}
