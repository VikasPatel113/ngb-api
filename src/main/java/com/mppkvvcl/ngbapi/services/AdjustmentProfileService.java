package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.AdjustmentProfileDAO;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentProfileInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by ANKIT on 19-08-2017.
 */
@Service
public class AdjustmentProfileService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    Logger logger  = GlobalResources.getLogger(AdjustmentProfileService.class);

    /**
     * Asking spring to inject dependency on AdjustmentProfileRepository.
     * So that we can use it to perform various operation through repo.
     */
    @Autowired
    AdjustmentProfileDAO adjustmentProfileDAO;

    /**
     * Asking spring to get Singleton object of AdjustmentService.
     */
    @Autowired
    AdjustmentService adjustmentService;

    @Transactional(rollbackFor = Exception.class)
    public AdjustmentProfileInterface insert(AdjustmentProfileInterface adjustmentProfile){
        String methodName = "insert() : ";
        logger.info(methodName+" called");
        adjustmentProfile.setUpdatedOn(GlobalResources.getCurrentDate());
        return adjustmentProfileDAO.add(adjustmentProfile);
    }

    public List<AdjustmentProfileInterface> getByAdjustmentId(long adjustmentId){
        String methodName = "getByAdjustmentId() : ";
        logger.info(methodName + "called");
        return adjustmentProfileDAO.getByAdjustmentId(adjustmentId);
    }

    @Transactional(rollbackFor = Exception.class)
    public AdjustmentInterface update(AdjustmentProfileInterface adjustmentProfile) throws Exception {
        String methodName = "update() : ";
        logger.info(methodName + "called");
        boolean status = false;
        AdjustmentInterface adjustment =  null;
        AdjustmentInterface updatedAdjustment =  null;
        AdjustmentProfileInterface insertedAdjustmentProfile = null;
        if(adjustmentProfile != null){
            adjustmentProfile.setUpdatedOn(GlobalResources.getCurrentDate());
            insertedAdjustmentProfile = adjustmentProfileDAO.add(adjustmentProfile);
            if(insertedAdjustmentProfile != null){
                adjustment = adjustmentService.getById(insertedAdjustmentProfile.getAdjustmentId());
                status = checkStatus(insertedAdjustmentProfile.getAdjustmentId());
                if(adjustment != null){
                    if(status && adjustmentProfile.getStatus()){
                        adjustment.setApprovalStatus(AdjustmentInterface.APPROVED);
                        setUpdateDetails(adjustment);
                        updatedAdjustment = adjustmentService.update(adjustment);
                    }else if(!status && !adjustmentProfile.getStatus()){
                        adjustment.setApprovalStatus(AdjustmentInterface.REJECTED);
                        adjustment.setDeleted(AdjustmentInterface.DELETED_TRUE);
                        setUpdateDetails(adjustment);
                        updatedAdjustment = adjustmentService.update(adjustment);
                    }else{
                        updatedAdjustment = adjustment;
                    }
                }else{
                    logger.error(methodName +"Adjustment Not Found for Adjustment ID "+insertedAdjustmentProfile.getAdjustmentId());
                    throw new Exception("Unable to Update Adjustment");
                }
            }
        }else{
            logger.error("Adjustment Profile passed null");
        }
        return updatedAdjustment;
    }


    public boolean checkStatus(long adjustmentId){
        boolean status = true;
        List<AdjustmentProfileInterface> adjustmentProfiles = adjustmentProfileDAO.getByAdjustmentId(adjustmentId);
        for(AdjustmentProfileInterface adjustmentProfile : adjustmentProfiles){
            if(adjustmentProfile.getStatus() == false) {
                status = false;
            }
        }
        return status;
    }

    private void setUpdateDetails(AdjustmentInterface adjustment) {
        String methodName = "setUpdateDetails()  : ";
        if (adjustment != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            adjustment.setUpdatedOn(date);
            adjustment.setUpdatedBy(user);
        }
    }
}
