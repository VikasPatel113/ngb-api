package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.ConsumerConnectionInformationHistoryDAO;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationHistoryInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConsumerConnectionInformationHistoryService {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerConnectionInformationHistoryService.class);

    @Autowired
    private ConsumerConnectionInformationHistoryDAO consumerConnectionInformationHistoryDAO;

    @Autowired
    private GlobalResources globalResources;


    public ConsumerConnectionInformationHistoryInterface insert(ConsumerConnectionInformationHistoryInterface consumerConnectionInformationHistoryInterface) {
        String methodName = "insert() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationHistoryInterface insertedConsumerConnectionInformationInterface = null;
        if (consumerConnectionInformationHistoryInterface != null) {
            setAuditDetails(consumerConnectionInformationHistoryInterface);
            insertedConsumerConnectionInformationInterface = consumerConnectionInformationHistoryDAO.add(consumerConnectionInformationHistoryInterface);
        }
        return insertedConsumerConnectionInformationInterface;
    }
    public List<ConsumerConnectionInformationHistoryInterface> insert(List<ConsumerConnectionInformationHistoryInterface> consumerConnectionInformationHistoryInterfaces) {
        final String methodName = "insert() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationHistoryInterface> insertedConsumerConnectionInformationHistoryInterfaces = null;
        if (consumerConnectionInformationHistoryInterfaces != null) {
            insertedConsumerConnectionInformationHistoryInterfaces = new ArrayList<>();
            for (ConsumerConnectionInformationHistoryInterface ccih : consumerConnectionInformationHistoryInterfaces) {
                setAuditDetails(ccih);
                ConsumerConnectionInformationHistoryInterface insertedConsumerConnectionInformationHistoryInterface = insert(ccih);
                if (insertedConsumerConnectionInformationHistoryInterface != null) {
                    insertedConsumerConnectionInformationHistoryInterfaces.add(insertedConsumerConnectionInformationHistoryInterface);
                }
            }
        }
        return insertedConsumerConnectionInformationHistoryInterfaces;
    }

    private void setAuditDetails(ConsumerConnectionInformationHistoryInterface consumerConnectionInformationHistoryInterface) {
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (consumerConnectionInformationHistoryInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            consumerConnectionInformationHistoryInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            consumerConnectionInformationHistoryInterface.setCreatedOn(GlobalResources.getCurrentDate());
        }
    }
}
