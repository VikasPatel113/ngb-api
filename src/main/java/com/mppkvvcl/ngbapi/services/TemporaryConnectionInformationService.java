package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TemporaryConnectionInformationInterface;
import com.mppkvvcl.ngbdao.daos.TemporaryConnectionInformationDAO;
import com.mppkvvcl.ngbentity.beans.TemporaryConnectionInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;

/**
 * Created by SUMIT on 15-06-2017.
 */
@Service
public class TemporaryConnectionInformationService {
    /**
     * Asking Spring to inject TemporaryConnectionInformationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on temporary connection information table
     * at the backend Database
     */
    @Autowired
    private TemporaryConnectionInformationDAO temporaryConnectionInformationDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(TemporaryConnectionInformationService.class);

    /**
     * This insert method takes temporaryConnectionInformation object and insert it into the TemporaryConnectionInformation table in the backend database.<br>
     * Return insertedTemporaryConnectionInformation if successful else return null.<br><br>
     * param temporaryConnectionInformation<br><br>
     * return TemporaryConnectionInformation<br>
     */
    public TemporaryConnectionInformationInterface insert(TemporaryConnectionInformation temporaryConnectionInformation){
        String methodName = " insert : ";
        TemporaryConnectionInformationInterface insertedTemporaryConnectionInformation = null;
        logger.info(methodName + "Started insertion for TemporaryConnectionInformation");
        if (temporaryConnectionInformation != null){
            logger.info(methodName + "Calling TemporaryConnectionInformationRepository for inserting TemporaryConnectionInformation");
            insertedTemporaryConnectionInformation = temporaryConnectionInformationDAO.add(temporaryConnectionInformation);
            if (insertedTemporaryConnectionInformation != null){
                logger.info(methodName + "Successfull inserted TemporaryConnectionInformation");
            }else{
                logger.error(methodName + "unable to insert TemporaryConnectionInformation row as TemporaryConnectionInformationRepository sending null");
            }
        }else{
            logger.error(methodName + "Received TemporaryConnectionInformation object is null");
        }
        return insertedTemporaryConnectionInformation;
    }
}
