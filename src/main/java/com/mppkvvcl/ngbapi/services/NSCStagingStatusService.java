package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.utility.NGBAPIUtility;
import com.mppkvvcl.ngbdao.daos.NSCStagingStatusDAO;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Created by NITISH on 31-05-2017.
 * Service class for NSCStagingStatus Table at the backend database to perform
 * various business logic in the application.
 */
@Service
public class NSCStagingStatusService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(NSCStagingStatusService.class);

    @Autowired
    private GlobalResources globalResources;

    /**
     * Asking Spring to inject NSCStagingStatusRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on NSCStagingStatus table
     * at the backend Database
     */
    @Autowired
    private NSCStagingStatusDAO nscStagingStatusDAO;

    /**
     * This insert method takes an NSCStagingStatus class object and insert its data in the
     * backend table of nsc_staging_status at the database. On successful insertion it returns
     * the newly inserted object with id; if insertion fails it returns null,<br><br>
     * param nscStagingStatus<br>
     * return NSCStaging<br>
     */
    public NSCStagingStatusInterface insert(NSCStagingStatusInterface nscStagingStatus) {
        String methodName = "insert : ";
        logger.info(methodName + "called");
        NSCStagingStatusInterface insertedNSCStagingStatus = null;
        if (nscStagingStatus != null) {
            setAuditDetails(nscStagingStatus);
            insertedNSCStagingStatus = nscStagingStatusDAO.add(nscStagingStatus);
        }
        return insertedNSCStagingStatus;
    }

    /**
     * This getById method takes an id and fetches the NSCStagingStatus object against that id from the backend
     * database using the injected NSCStagingStatusRepository object and returns it if found else returns null.<br><br>
     * param id<br>
     * return NSCStagingStatus<br>
     */
    public NSCStagingStatusInterface getById(Long id) {
        String methodName = "getById: ";
        logger.info(methodName + " started");
        NSCStagingStatusInterface nscStagingStatus = nscStagingStatusDAO.getById(id);
        return nscStagingStatus;
    }

    /**
     * This getByNSCStagingId method takes an nscStagingId and fetches the NSCStagingStatus object against that nscStagingId from the backend
     * database using the injected NSCStagingStatusRepository object and returns it if found else returns null<br><br>
     * param nscStagingId<br>
     * return NSCStagingStatus<br>
     */
    public NSCStagingStatusInterface getByNSCStagingId(long nscStagingId) {
        String methodName = "getByNSCStagingId : ";
        logger.info(methodName + "started");
        NSCStagingStatusInterface nscStagingStatus = nscStagingStatusDAO.getByNscStagingId(nscStagingId);
        return nscStagingStatus;
    }

    /**
     * method to check whether a passed meterIdentifier exist in any pending nsc application.
     * It return nscStagingStatus if successful else return null.<br><br>
     * param status<br>
     * param meterIdentifier<br>
     * return nscStagingStatus
     */
    public NSCStagingStatusInterface getByPendingStatusAndMeterIdentifier(String status, String meterIdentifier) {
        String methodName = "getByStatusAndMeterIdentifier() : ";
        logger.info(methodName + "called with status " + status + " identifier : " + meterIdentifier);
        NSCStagingStatusInterface nscStagingStatus = null;
        if (status != null && meterIdentifier != null) {
            logger.info(methodName + "fetching nscstagingstatus with status " + status + " and meterIdentifer as " + meterIdentifier);
            nscStagingStatus = nscStagingStatusDAO.getByPendingStatusAndMeterIdentifier(status, meterIdentifier);
            logger.info(methodName + "fetched nscstagingstatus with status " + status + " and meterIdentifer as " + meterIdentifier + " as : " + nscStagingStatus);
        }
        return nscStagingStatus;
    }

    public List<NSCStagingStatusInterface> getByLocationCodeAndStatus(String locationCode, String status) {
        String methodName = "getByLocationCodeAndStatus() : ";
        logger.info(methodName + "called status " + status + " location : " + locationCode);
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if (locationCode != null && status != null) {
            nscStagingStatusInterfaces = nscStagingStatusDAO.getByLocationCodeAndStatus(locationCode, status);
        }
        return nscStagingStatusInterfaces;
    }

    public List<NSCStagingStatusInterface> getByLocationCode(String locationCode) {
        String methodName = "getByLocationCode() : ";
        logger.info(methodName + "called for " + locationCode);
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if (locationCode != null) {
            nscStagingStatusInterfaces = nscStagingStatusDAO.getByLocationCode(locationCode);
        }
        return nscStagingStatusInterfaces;
    }

    public List<NSCStagingStatusInterface> getByGroupNoAndStatus(String groupNo, String status) {
        String methodName = "getByGroupNoAndStatus() : ";
        logger.info(methodName + "called status " + status + " groupNo : " + groupNo);
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if (groupNo != null && status != null) {
            nscStagingStatusInterfaces = nscStagingStatusDAO.getByGroupNoAndStatus(groupNo, status);
        }
        return nscStagingStatusInterfaces;
    }

    public List<NSCStagingStatusInterface> getByGroupNo(String groupNo) {
        String methodName = "getByGroupNo() : ";
        logger.info(methodName + "called groupNo : " + groupNo);
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if (groupNo != null) {
            nscStagingStatusInterfaces = nscStagingStatusDAO.getByGroupNo(groupNo);
        }
        return nscStagingStatusInterfaces;
    }

    //Count methods coded by nitish
    public long getCountByLocationCode(String locationCode) {
        final String methodName = "getCountByLocationCode() : ";
        logger.info(methodName + "called");
        return nscStagingStatusDAO.getCountByLocationCode(locationCode);
    }

    public long getCountByLocationCodeAndStatus(String locationCode, String status) {
        final String methodName = "getCountByLocationCodeAndStatus() : ";
        logger.info(methodName + "called");
        return nscStagingStatusDAO.getCountByLocationCodeAndStatus(locationCode, status);
    }

    public long getCountByGroupNo(String groupNo) {
        final String methodName = "getCountByGroupNo() : ";
        logger.info(methodName + "called");
        return nscStagingStatusDAO.getCountByGroupNo(groupNo);
    }

    public long getCountByGroupNoAndStatus(String groupNo, String status) {
        final String methodName = "getCountByGroupNoAndStatus() : ";
        logger.info(methodName + "called");
        long count = nscStagingStatusDAO.getCountByGroupNoAndStatus(groupNo, status);
        return count;
    }

    //Pageable Methods coded by nitish
    public List<NSCStagingStatusInterface> getByLocationCodeWithPagination(String locationCode, String sortBy, String sortOrder, int pageNumber, int pageSize) {
        String methodName = "getByLocationCode() : ";
        logger.info(methodName + "called with pagination for " + locationCode);
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if (locationCode != null && pageNumber >= 0) {
            Pageable pageable = null;
            if (sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)) {
                pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.ASC, sortBy);
            } else {
                pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.DESC, sortBy);
            }
            nscStagingStatusInterfaces = nscStagingStatusDAO.getByLocationCode(locationCode, pageable);
        }
        return nscStagingStatusInterfaces;
    }

    public List<NSCStagingStatusInterface> getByLocationCodeAndStatusWithPagination(String locationCode, String status, String sortBy, String sortOrder, int pageNumber, int pageSize) {
        String methodName = "getByLocationCodeAndStatusWithPagination() : ";
        logger.info(methodName + "called " + locationCode);
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if (locationCode != null && status != null && pageNumber >= 0) {
            Pageable pageable = null;
            if (sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)) {
                pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.ASC, sortBy);
            } else {
                pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.DESC, sortBy);
            }
            nscStagingStatusInterfaces = nscStagingStatusDAO.getByLocationCodeAndStatus(locationCode, status, pageable);
        }
        return nscStagingStatusInterfaces;
    }

    public List<NSCStagingStatusInterface> getByGroupNoWithPagination(String groupNo, String sortBy, String sortOrder, int pageNumber, int pageSize) {
        String methodName = "getByGroupNoWithPagination() : ";
        logger.info(methodName + "called " + groupNo);
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if (groupNo != null && pageNumber >= 0) {
            Pageable pageable = null;
            if (sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)) {
                pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.ASC, sortBy);
            } else {
                pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.DESC, sortBy);
            }
            nscStagingStatusInterfaces = nscStagingStatusDAO.getByGroupNo(groupNo, pageable);
        }
        return nscStagingStatusInterfaces;
    }

    public List<NSCStagingStatusInterface> getByGroupNoAndStatusWithPagination(String groupNo, String status, String sortBy, String sortOrder, int pageNumber, int pageSize) {
        String methodName = "getByGroupNoAndStatusWithPagination() : ";
        logger.info(methodName + "called " + groupNo + " " + status);
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if (groupNo != null && status != null && pageNumber >= 0) {
            Pageable pageable = null;
            if (sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)) {
                pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.ASC, sortBy);
            } else {
                pageable = new PageRequest(pageNumber, pageSize, Sort.Direction.DESC, sortBy);
            }
            nscStagingStatusInterfaces = nscStagingStatusDAO.getByGroupNoAndStatus(groupNo, status, pageable);
        }
        return nscStagingStatusInterfaces;
    }

    private void setAuditDetails(NSCStagingStatusInterface nscStagingStatusInterface) {
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (nscStagingStatusInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            nscStagingStatusInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            nscStagingStatusInterface.setCreatedOn(GlobalResources.getCurrentDate());
            nscStagingStatusInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            nscStagingStatusInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }

    private void setUpdateAuditDetails(NSCStagingStatusInterface nscStagingStatusInterface) {
        final String methodName = "setUpdateAuditDetails() : ";
        logger.info(methodName + "called");
        if (nscStagingStatusInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            nscStagingStatusInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            nscStagingStatusInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }

    /**
     * method to check whether a passed BPL No exist in any pending nsc application.
     * It return nscStagingStatus if successful else return null.<br><br>
     * param status<br>
     * param bplNo<br>
     * return nscStagingStatus
     */
    public NSCStagingStatusInterface getByPendingStatusAndBPlNo(String status, String bplNo) {
        String methodName = "getByPendingStatusAndBPlNo() : ";
        logger.info(methodName + "called with status " + status + " BPL no : " + bplNo);
        NSCStagingStatusInterface nscStagingStatusInterface = null;
        if (status != null && bplNo != null) {
            nscStagingStatusInterface = nscStagingStatusDAO.getByStatusAndBPlNo(status, bplNo);
        }
        return nscStagingStatusInterface;
    }

    /**
     * method to check whether a passed Employee No exist in any pending nsc application.
     * It return nscStagingStatus if successful else return null.<br><br>
     * param status<br>
     * param employeeNo<br>
     * return nscStagingStatus
     */
    public NSCStagingStatusInterface getByPendingStatusAndEmployeeNo(String status, String employeeNo) {
        String methodName = "getByPendingStatusAndEmployeeNo() : ";
        logger.info(methodName + "called with status " + status + " Employee no : " + employeeNo);
        NSCStagingStatusInterface nscStagingStatusInterface = null;
        if (status != null && employeeNo != null) {
            nscStagingStatusInterface = nscStagingStatusDAO.getByStatusAndEmployeeNo(status, employeeNo);
        }
        return nscStagingStatusInterface;
    }
}


