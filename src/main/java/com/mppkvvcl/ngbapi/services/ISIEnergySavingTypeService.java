package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ISIEnergySavingTypeInterface;
import com.mppkvvcl.ngbdao.daos.ISIEnergySavingTypeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SUMIT on 15-06-2017.
 */
@Service
public class ISIEnergySavingTypeService {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ISIEnergySavingTypeService.class);

    /**
     * Asking Spring to inject ConsumerGovernmentMappingRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer government mapping  table
     * at the backend Database
     */
    @Autowired
    private ISIEnergySavingTypeDAO isiEnergySavingTypeDAO;

    /**
     * This insert method is used to insert isiEnergySavingType object to
     * respective table isi.<br><br>
     * param isiEnergySavingType<br>
     * return insertedIsiEnergySavingType<br>
     */
    public ISIEnergySavingTypeInterface insert(ISIEnergySavingTypeInterface isiEnergySavingType){
        String methodName = "insert : " ;
        ISIEnergySavingTypeInterface insertedIsiEnergySavingType = null ;
        logger.info(methodName + "Started insertion for ISIEnergySavingType ");
        if(isiEnergySavingType != null){
            logger.info(methodName + "Calling ISIEnergySavingType for inserting isiEnergySavingType");
            insertedIsiEnergySavingType = isiEnergySavingTypeDAO.add(isiEnergySavingType);
            if(insertedIsiEnergySavingType != null){
                logger.info(methodName + "Succesfully inserted ISIEnergySavingType.");
            }else{
                logger.error(methodName + "unable to insert isienergy saving type row. Repository sending null");
            }
        }else{
            logger.error(methodName + "Received isiEnergySavingType object is null");
        }
        return insertedIsiEnergySavingType;
    }

    public List<? extends ISIEnergySavingTypeInterface> getAll(){
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        return isiEnergySavingTypeDAO.getAll();
    }
}
