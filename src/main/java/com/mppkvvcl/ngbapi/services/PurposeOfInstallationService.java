package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PurposeOfInstallationInterface;
import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;
import com.mppkvvcl.ngbdao.daos.PurposeOfInstallationDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NITISH on 25-05-2017.
 * Service class for PurposeOfInstallation Table at the backend database to perform
 * various business logic in the application.
 *
 * Edited  by SUMIT on 26-05-2017
 * changed tariffCategory in repo methods to tariffCode as per discussions done on 26-05-2017
 */
@Service
public class PurposeOfInstallationService {

    /**
     * Asking Spring to inject PurposeOfInstallationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on PurposeOfInstallation table
     * at the backend Database
     */
    @Autowired
    private PurposeOfInstallationDAO purposeOfInstallationDAO;

    /**
     * Asking Spring to inject TariffService dependency in the below variable
     * so that we can use various service method under that class
     */
    @Autowired
    private TariffService tariffService;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(PurposeOfInstallationService.class);

    /**
     * getPurposeOfInstallationsByTariffCode method provides services to fetch details to get data by Tariff Code.<br><br>
     * param tariffCode<br>
     * return purposeOfInstallations<br>
     *
     */
    public List<PurposeOfInstallationInterface> getPurposeOfInstallationsByTariffCode(String tariffCode) {
        logger.info("getPurposeOfInstallationsByTariffCode : getPurposeOfInstallationsByTariffCode called for tariffCode: " + tariffCode);
        List<PurposeOfInstallationInterface> purposeOfInstallations = null;
        if (tariffCode != null) {
            purposeOfInstallations = purposeOfInstallationDAO.getByTariffCode(tariffCode);
            if (purposeOfInstallations != null) {
                logger.info("getPurposeOfInstallationsByTariffCode : Successfully retrieved " + purposeOfInstallations.size() + " purpose of installations in getPurposeOfInstallationsByTariffCode method" +
                        " for passed tariffCode: " + tariffCode);
            }
        } else {
            logger.error("getPurposeOfInstallationsByTariffCode : tariffCode passed is null in getPurposeOfInstallationsByTariffCode method.");
        }
        return purposeOfInstallations;
    }

    /**
     * Created by SUMIT dated 26-05-2017<br><br>
     *
     * getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType method provides list of purpose of installations for input params as above.
     * Please note that the tariffs corresponding to tariffCategory are being fetched on the basis of current server DATE.So any implementation should take this
     * in mind.<br><br>
     *
     * param tariffCategory<br>
     * param meteringStatus<br>
     * param connectionType<br>
     * return purposeOfInstallations<br>
     *
     */
    public List<PurposeOfInstallationInterface> getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType(String tariffCategory, String meteringStatus, String connectionType) {
        logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : method called for tariffCategory : "+tariffCategory+" "+meteringStatus+" "+connectionType);
        List<PurposeOfInstallationInterface> purposeOfInstallations = null;
        if(tariffCategory != null && meteringStatus != null && connectionType != null){
            List<TariffInterface> tariffs = tariffService.getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate(tariffCategory,meteringStatus,connectionType,null);
            if (tariffs != null){
                logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : Successfully retreived "+tariffs.size()+" tariffs." +
                        "for passed tariffCategory  "+tariffCategory+" "+"meteringStatus "+meteringStatus+" "+connectionType);
                if(tariffs.size() > 0){
                    purposeOfInstallations = new ArrayList<>();
                    logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : fetching purpose of installation for tariffs "+tariffs.size());
                    for(TariffInterface tariff : tariffs){
                        List<PurposeOfInstallationInterface> purposes = getPurposeOfInstallationsByTariffCode(tariff.getTariffCode());
                        if(purposes != null){
                            if(purposes.size() > 0){
                                logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : Got "+ purposes.size() +" purposes for tariffCode " + tariff.getTariffCode());
                                purposeOfInstallations.addAll(purposes);
                            }else{
                                logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : Got "+ purposes.size() +" purposes for tariffCode " + tariff.getTariffCode());
                            }
                        }
                    }
                }else{
                    logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : Got "+tariffs.size()+"  tariffs, hence returning ");
                }
            }else{
                logger.error("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : tariffs is null.");
            }
        }
        return purposeOfInstallations;
    }


    /**
     * Created by SUMIT dated 26-05-2017<br><br>
     *
     * getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType method provides list of purpose of installations for input params as above.
     * Please note that the tariffs corresponding to tariffCategory are being fetched on the basis of current server DATE.So any implementation should take this
     * in mind.<br><br>
     *
     * param tariffCategory<br>
     * param meteringStatus<br>
     * param connectionType<br>
     * return purposeOfInstallations<br>
     *
     */
    public List<PurposeOfInstallationInterface> getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffType(String tariffCategory, String meteringStatus, String connectionType, String tariffType) {
        logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : method called for tariffCategory : "+tariffCategory+" "+meteringStatus+" "+connectionType+ "" + tariffType);
        List<PurposeOfInstallationInterface> purposeOfInstallations = null;
        if(tariffCategory != null && meteringStatus != null && connectionType != null && tariffType != null){
            List<TariffInterface> tariffs=tariffService.getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate(tariffCategory,meteringStatus,connectionType,tariffType,null);
            if (tariffs != null){
                logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : Successfully retreived "+tariffs.size()+" tariffs." +
                        "for passed tariffCategory  "+tariffCategory+" "+"meteringStatus "+meteringStatus+" "+connectionType +" tariffType " + tariffType);
                if(tariffs.size() > 0){
                    purposeOfInstallations = new ArrayList<>();
                    logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : fetching purpose of installation for tariffs "+tariffs.size());
                    for(TariffInterface tariff : tariffs){
                        List<PurposeOfInstallationInterface> purposes = getPurposeOfInstallationsByTariffCode(tariff.getTariffCode());
                        if(purposes != null){
                            if(purposes.size() > 0){
                                logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : Got "+ purposes.size() +" purposes for tariffCode " + tariff.getTariffCode());
                                purposeOfInstallations.addAll(purposes);
                            }else{
                                logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : Got "+ purposes.size() +" purposes for tariffCode " + tariff.getTariffCode());
                            }
                        }
                    }
                }else{
                    logger.info("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : Got "+tariffs.size()+"  tariffs, hence returning ");
                }
            }else{
                logger.error("getPurposeOfInstallationByTariffCategoryAndMeteringStatusAndConnectionType : tariffs is null.");
            }
        }
        return purposeOfInstallations;
    }

    public PurposeOfInstallationInterface getPurposeOfInstallationById(long id) throws Exception {
        String methodName = "getPurposeOfInstallationById() : ";
        logger.info(methodName + " called for id : "+ id);
        PurposeOfInstallationInterface purposeOfInstallation = null;
         purposeOfInstallation = purposeOfInstallationDAO.getById(id);
         if(purposeOfInstallation == null){
             logger.error(methodName + " Purpose Of installation found null . Please check the id ");
             throw new Exception("Purpose of installation not found ");
         }

        return purposeOfInstallation;
    }
}
