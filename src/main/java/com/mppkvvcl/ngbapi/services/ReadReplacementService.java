package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.CustomMeterReplacement;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by PREETESH on 7/10/2017.
 */
@Service
public class ReadReplacementService {

    private static final Logger logger = GlobalResources.getLogger(ReadReplacementService.class);

    @Autowired
    private ConsumerInformationService consumerInformationService;

    @Autowired
    private ConsumerMeterMappingService consumerMeterMappingService;

    @Autowired
    private ReadMasterService readMasterService;

    @Autowired
    private MeterCTRMappingService meterCTRMappingService;

    @Autowired
    private  ConsumerNoMasterService consumerNoMasterService;

    @Autowired
    private TariffDetailService tariffDetailService;

    @Autowired
    private TariffChangeDetailService tariffChangeDetailService;

    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;


    public CustomMeterReplacement getCustomMeterReplacementByConsumerNo(String consumerNo) throws Exception {
        String methodName = " getCustomMeterReplacementByConsumerNo() : ";
        CustomMeterReplacement customMeterReplacement = null;
        ConsumerInformationInterface selectedConsumerInformation = null;
        BigDecimal selectedMF = null;
        MeterMasterInterface selectedMeterMaster = null;
        CTRMasterInterface selectedCTRMaster = null;
        ReadMasterInterface selectedReadMaster = null;
        TariffDetailInterface tariffDetail = null;
        TariffChangeDetailInterface tariffChangeDetail = null;
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        ConsumerNoMasterInterface consumerNoMaster = null;
        logger.info(methodName+"called");
        if (consumerNo == null) {
            logger.error(methodName + " consumer no passed is null, consumer no: " + consumerNo);
            throw new Exception("consumer no passed is null");
        }
        consumerNo = consumerNo.trim();
        logger.info(methodName+ "fetching consumer no master");
        consumerNoMaster = consumerNoMasterService.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null) {
            logger.error(methodName + "Given consumer no doesn't exist in  ConsumerNoMaster");
            throw new Exception("Given consumer no doesn't exist in  ConsumerNoMaster");
        }
        logger.info(methodName + "fetched one row for consumerNoMaster" + consumerNoMaster);
        String status = consumerNoMaster.getStatus();
        if (status == null) {
            logger.error(methodName + "status found null for given consumer no :" + consumerNo);
            throw new Exception("status found null for given consumer no ");
        }
        //Below validation Checks whether consumer is INACTIVE (PDC)
        if (status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)) {
            logger.error(methodName + "Consumer is INACTIVE consumer No:" + consumerNo);
            throw new Exception("Consumer is inactive");
        }
        logger.info(methodName + "Fetching Tariff Detail for Consumer no: " + consumerNo);
        tariffDetail = tariffDetailService.getLatestTariffByConsumerNo(consumerNo);
        if (tariffDetail == null) {
            logger.error(methodName + "No tariff details found for Consumer no:" + consumerNo);
            throw new Exception("No tariff details found for Consumer");
        }
        tariffChangeDetail = tariffChangeDetailService.getByTariffDetailId(tariffDetail.getId());
        if (tariffChangeDetail != null) {
            logger.info(methodName+"tariff changed for consumer, tariff change detail exist for consumer no:"+consumerNo);
            if(tariffChangeDetail.getMeteringStatus().equals(ConsumerConnectionInformationInterface.METERING_STATUS_UNMETERED)){
                logger.info(methodName+"consumer is of un-metered type");
                throw new Exception("consumer is of un-metered type");
            }
            logger.info(methodName+"consumer is of metered type , proceeding for fetch reading");
        }else {
            logger.info(methodName + "Tariff not changed previously, fetching consumer connection information for consumer no :"+consumerNo);
            consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
            if (consumerConnectionInformation == null) {
                logger.error(methodName + "Consumer connection information not found for consumer no :" + consumerNo);
                throw new Exception("Consumer connection information not found for consumer ");
            }
            logger.info(methodName+ "fetched consumer connection information for consumer no :"+consumerNo);
            if(consumerConnectionInformation.getMeteringStatus().equals(ConsumerConnectionInformationInterface.METERING_STATUS_UNMETERED)){
                logger.info(methodName+"consumer is of un-metered type ");
                throw new Exception("consumer is of un-metered type ");
            }
            logger.info(methodName+"consumer is of metered type , proceeding for fetch reading");
        }
        selectedConsumerInformation = consumerInformationService.getConsumerInformationByConsumerNo(consumerNo);
        logger.info(methodName + " Consumer Information retrieved: " + selectedConsumerInformation);
        if (selectedConsumerInformation == null) {
            logger.error(methodName + " consumer information not found for consumer no:" + consumerNo);
            throw new Exception("consumer information not found");
        }
        selectedMF = consumerMeterMappingService.getMFByConsumerNoForActiveMeter(consumerNo);
        logger.info(methodName + " MF retrieved: " + selectedMF);
        if (selectedMF == null) {
            logger.error(methodName + " Applied mf not found for consumer no:" + consumerNo);
            throw new Exception("Applied mf not found for consumer");

        }
        selectedMeterMaster = consumerMeterMappingService.getActiveMeterByConsumerNo(consumerNo);
        logger.info(methodName + "Meter Master Retrieved: " + selectedMeterMaster);
        if (selectedMeterMaster == null) {
            logger.error(methodName + "Meter master not found for given consumer");
            throw new Exception("Meter master not found for given consumer");
        }
        String typeCode = selectedMeterMaster.getCode();
        String meterIdentifier = selectedMeterMaster.getIdentifier();
        if (typeCode.equals(MeterMasterInterface.CODE_TYPE_CTT)) {
            selectedCTRMaster = meterCTRMappingService.getActiveCTRMasterByMeterIdentifier(meterIdentifier);
        } else {
            if (typeCode.equals(MeterMasterInterface.CODE_TYPE_WCS) || typeCode.equals(MeterMasterInterface.CODE_TYPE_WCT)) {
                logger.info(methodName + "CTR not selected ");
            } else {
                logger.error(methodName + "No Correct Type found for meter: " + selectedMeterMaster);
                throw new Exception("No Correct Type found for meter");
            }
        }
        selectedReadMaster = readMasterService.getLatestReadingByConsumerNo(consumerNo);
        logger.info(methodName + " Got latest reading as " + selectedReadMaster);
        if (selectedReadMaster == null) {
            logger.error(methodName + " previous Reading not retrieved ");
            throw new Exception("previous Reading not retrieved");
        }
        customMeterReplacement = new CustomMeterReplacement();
        customMeterReplacement.setConsumerInformation(selectedConsumerInformation);
        customMeterReplacement.setMf(selectedMF);
        customMeterReplacement.setMeterMaster(selectedMeterMaster);
        customMeterReplacement.setReadMaster(selectedReadMaster);
        if (selectedCTRMaster == null) {
            logger.error(methodName + "CTR not found ");
        }
        customMeterReplacement.setCtrMaster(selectedCTRMaster);
        logger.info(methodName + "customMeterReplacement set successfully" + customMeterReplacement);
        return customMeterReplacement;
    }
}
