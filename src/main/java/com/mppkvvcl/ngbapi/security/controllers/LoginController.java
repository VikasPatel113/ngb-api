package com.mppkvvcl.ngbapi.security.controllers;

import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "authentication/login")
public class  LoginController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(LoginController.class);

    @Autowired
    private UserDetailService userDetailService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity authenticated(@RequestAttribute("username") String username) throws Exception{
        String methodName = "authenticated() : ";
        logger.info(methodName + "called");
        logger.info(methodName + "Authentication Successful for user: " + username);
        ResponseEntity<?> responseEntity = null;
        if(username != null) {
            UserDetail userDetail = userDetailService.getByUsername(username);
            if (userDetail != null) {
                //For Time Being not sending user with response
                //Details of the user is appended in Token
                responseEntity = new ResponseEntity(HttpStatus.OK);
                return responseEntity;
            }
        }
        responseEntity = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        return responseEntity;
    }
}