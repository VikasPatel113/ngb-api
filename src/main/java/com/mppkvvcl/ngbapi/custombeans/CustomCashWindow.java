package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.CashWindowStatusInterface;
import com.mppkvvcl.ngbinterface.interfaces.UserDetailInterface;

/**
 * Created by PREETESH on 7/25/2017.
 */
public class CustomCashWindow {

    private CashWindowStatusInterface cashWindowStatus;
    public UserDetailInterface userDetail;
    private long cashTotalAmount;
    private long noOfPaymentsInCash;
    private long chequeTotalAmount;
    private long noOfPaymentsInCheque;

    public UserDetailInterface getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetailInterface userDetail) {
        this.userDetail = userDetail;
    }

    public CashWindowStatusInterface getCashWindowStatus() {
        return cashWindowStatus;
    }

    public void setCashWindowStatus(CashWindowStatusInterface cashWindowStatus) {
        this.cashWindowStatus = cashWindowStatus;
    }

    public long getCashTotalAmount() {
        return cashTotalAmount;
    }

    public void setCashTotalAmount(long cashTotalAmount) {
        this.cashTotalAmount = cashTotalAmount;
    }

    public long getNoOfPaymentsInCash() {
        return noOfPaymentsInCash;
    }

    public void setNoOfPaymentsInCash(long noOfPaymentsInCash) {
        this.noOfPaymentsInCash = noOfPaymentsInCash;
    }

    public long getChequeTotalAmount() {
        return chequeTotalAmount;
    }

    public void setChequeTotalAmount(long chequeTotalAmount) {
        this.chequeTotalAmount = chequeTotalAmount;
    }

    public long getNoOfPaymentsInCheque() {
        return noOfPaymentsInCheque;
    }

    public void setNoOfPaymentsInCheque(long noOfPaymentsInCheque) {
        this.noOfPaymentsInCheque = noOfPaymentsInCheque;
    }
}
