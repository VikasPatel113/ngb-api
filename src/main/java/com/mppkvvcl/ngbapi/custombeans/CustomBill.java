package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.AgricultureBill6MonthlyInterface;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationInterface;

import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import com.mppkvvcl.ngbentity.beans.AgricultureBill6Monthly;

/**
 * Created by PREETESH on 7/31/2017.
 */
public class CustomBill {

    private ConsumerInformationInterface consumerInformation;
    private AgricultureBill6MonthlyInterface agricultureBill6Monthly;
    private BillInterface bill;
    private long totalBillAmount;
    private long partPaymentAmount;
    private long paymentReceived;
    private long balanceAmountToPay;

    public ConsumerInformationInterface getConsumerInformation() {
        return consumerInformation;
    }

    public void setConsumerInformation(ConsumerInformationInterface consumerInformation) {
        this.consumerInformation = consumerInformation;
    }

    public AgricultureBill6MonthlyInterface getAgricultureBill6Monthly() {
        return agricultureBill6Monthly;
    }

    public void setAgricultureBill6Monthly(AgricultureBill6MonthlyInterface agricultureBill6Monthly) {
        this.agricultureBill6Monthly = agricultureBill6Monthly;
    }

    public void setAgricultureBill6Monthly(AgricultureBill6Monthly agricultureBill6Monthly) {
        this.agricultureBill6Monthly = agricultureBill6Monthly;
    }

    public BillInterface getBill() {
        return bill;
    }

    public void setBill(BillInterface bill) {
        this.bill = bill;
    }

    public long getTotalBillAmount() {
        return totalBillAmount;
    }

    public void setTotalBillAmount(long totalBillAmount) {
        this.totalBillAmount = totalBillAmount;
    }

    public long getPartPaymentAmount() {
        return partPaymentAmount;
    }

    public void setPartPaymentAmount(long partPaymentAmount) {
        this.partPaymentAmount = partPaymentAmount;
    }

    public long getPaymentReceived() {
        return paymentReceived;
    }

    public void setPaymentReceived(long paymentReceived) {
        this.paymentReceived = paymentReceived;
    }

    public long getBalanceAmountToPay() {
        return balanceAmountToPay;
    }

    public void setBalanceAmountToPay(long balanceAmountToPay) {
        this.balanceAmountToPay = balanceAmountToPay;
    }

    @Override
    public String toString() {
        return "CustomBill{" +
                "bill=" + bill +
                ", totalBillAmount=" + totalBillAmount +
                ", partPaymentAmount=" + partPaymentAmount +
                ", paymentReceived=" + paymentReceived +
                ", balanceAmountToPay=" + balanceAmountToPay +
                '}';
    }
}
