package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.ReadMasterKW;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterKWInterface;

public class ReadMasterKWFactory {
    public static ReadMasterKWInterface build(){
        ReadMasterKWInterface readMasterKWInterface = new ReadMasterKW();
        return readMasterKWInterface;
    }
}
