package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.ConsumerConnectionInformationHistory;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationHistoryInterface;

public class ConsumerConnectionInformationHistoryFactory {
    public static ConsumerConnectionInformationHistoryInterface build() {
        ConsumerConnectionInformationHistoryInterface consumerConnectionInformationHistoryInterface = new ConsumerConnectionInformationHistory();
        return consumerConnectionInformationHistoryInterface;
    }
}
