package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.GMCAccounting;
import com.mppkvvcl.ngbinterface.interfaces.GMCAccountingInterface;

import java.math.BigDecimal;

public class GMCAccountingFactory {

    public static GMCAccountingInterface build() {
        GMCAccountingInterface gmcAccountingInterface = new GMCAccounting();
        return gmcAccountingInterface;
    }

    public static GMCAccountingInterface buildDefault(String consumerNo) {
        GMCAccountingInterface gmcAccountingInterface = new GMCAccounting();
        gmcAccountingInterface.setConsumerNo(consumerNo);
        gmcAccountingInterface.setCurrentBillMonth(null);
        gmcAccountingInterface.setCurrentConsumption(BigDecimal.ZERO);
        gmcAccountingInterface.setActualCumulativeConsumption(BigDecimal.ZERO);
        gmcAccountingInterface.setMinimumCumulative(BigDecimal.ZERO);
        gmcAccountingInterface.setHigherOfActualMinimumCumulative(BigDecimal.ZERO);
        gmcAccountingInterface.setAlreadyBilled(BigDecimal.ZERO);
        gmcAccountingInterface.setToBeBilled(BigDecimal.ZERO);
        gmcAccountingInterface.setPreviousMonth(null);
        gmcAccountingInterface.setPreviousConsumption(BigDecimal.ZERO);
        gmcAccountingInterface.setPreviousActualCumulativeConsumption(BigDecimal.ZERO);
        gmcAccountingInterface.setPreviousMinimumCumulative(BigDecimal.ZERO);
        gmcAccountingInterface.setPreviousHigherOfActualMinimumCumulative(BigDecimal.ZERO);
        gmcAccountingInterface.setPreviousAlreadyBilled(BigDecimal.ZERO);
        gmcAccountingInterface.setPreviousToBeBilled(BigDecimal.ZERO);
        return gmcAccountingInterface;
    }
}
