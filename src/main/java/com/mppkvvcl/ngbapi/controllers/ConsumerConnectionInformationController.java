package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.ConsumerConnectionInformationService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "consumer-connection-information")
public class ConsumerConnectionInformationController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerConnectionInformationController.class);

    /**
     * Asking spring to inject ConsumerConnectionInformationService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on ConsumerConnectionInformationService table data at the backend database.
     */
    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;

    @RequestMapping(value = "/consumer-no/{consumerNo}",method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getByConsumerNo(@PathVariable("consumerNo") String consumerNo){
        String methodName = "getByConsumerNo()";
        ResponseEntity response = null;
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        logger.info(methodName + " Got request to fetch Consumer connection information details with consumer no : " + consumerNo);
        if (consumerNo != null){
            logger.info(methodName + "Calling Consumer Connection Service to fetch details");
            consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
            if (consumerConnectionInformation != null){
                logger.info(methodName + "Fetched consumer connection information successfully. ");
                response = new ResponseEntity(consumerConnectionInformation,HttpStatus.OK);
            }else {
                logger.error(methodName + "Couldn't retrieve consumer connection information due some error");
                response = new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + " Got Consumer no is null.");
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
