package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.BPLConnectionMappingService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.BPLConnectionMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/bpl/connection/mapping")
public class BPLConnectionMappingController {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(BPLConnectionMappingController.class);

    @Autowired
    private BPLConnectionMappingService bplConnectionMappingService;

    @RequestMapping(method = RequestMethod.GET,value ="/consumer-no/{consumerNo}/status/{status}",produces ="application/json")
    public ResponseEntity getByConsumerNoAndStatus(@PathVariable("consumerNo") String consumerNo, @PathVariable("status") String status){
        String methodName = "getByConsumerNoAndStatus() : ";
        logger.info(methodName + "called "+consumerNo);
        List<BPLConnectionMappingInterface> bplConnectionMappingInterfaces = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if(consumerNo != null && status != null) {
            bplConnectionMappingInterfaces = bplConnectionMappingService.getByConsumerNoAndStatus(consumerNo,status);
            if (bplConnectionMappingInterfaces != null && bplConnectionMappingInterfaces.size() > 0) {
                response = new ResponseEntity<>(bplConnectionMappingInterfaces, HttpStatus.OK);
            } else {
                errorMessage = new ErrorMessage("No BPL Connection Mapping exist for consumer no: " + consumerNo+ " And status: "+status);
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + "Got input parameter is  null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET,value ="/bpl-no/{bplNo}/status/{status}",produces ="application/json")
    public ResponseEntity getByBplNoAndStatus(@PathVariable("bplNo") String bplNo, @PathVariable("status") String status){
        String methodName = "getByBplNoAndStatus() : ";
        logger.info(methodName + "called " + bplNo);
        List<BPLConnectionMappingInterface> bplConnectionMappingInterfaces = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if(bplNo != null && status != null) {
            bplConnectionMappingInterfaces = bplConnectionMappingService.getByBplNoAndStatus(bplNo,status);
            if (bplConnectionMappingInterfaces != null && bplConnectionMappingInterfaces.size() > 0) {
                response = new ResponseEntity<>(bplConnectionMappingInterfaces, HttpStatus.OK);
            } else {
                errorMessage = new ErrorMessage("No BPL Connection Mapping exist for consumer no: " + bplNo+ " And status: "+status);
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + "Got input parameter is  null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
