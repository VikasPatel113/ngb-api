package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.BankMasterService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.BankMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by ANSHIKA on 27-07-2017.
 */
@RestController
@RequestMapping(value = "/bank/master")
public class BankMasterController {
    /**
     * Requesting Spring to inject singleton BankMasterService object.
     */
    @Autowired
    BankMasterService bankMasterService;
    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(BankMasterController.class);

    /**
     * URI - /bank/master<br><br>
     * This method fetches the list of bankMasters when user hit the URI.<br><br>
     * Response 200 for OK status and list of bankMasters is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName + "is called to get all bankMasters");
        ResponseEntity<?> response = null;
        List<? extends BankMasterInterface> bankMasters = null;
        logger.info(methodName + "Calling BankMasterService method to view all bank master");
        bankMasters = bankMasterService.getAll();
        if(bankMasters != null){
            if(bankMasters.size() > 0){
                logger.info(methodName + "List of bankMasters found successfully");
                response = new ResponseEntity<>(bankMasters, HttpStatus.OK);
            }else{
                logger.error(methodName + "No content found in list");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else{
            logger.error(methodName + "List of bankMasters not found");
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
