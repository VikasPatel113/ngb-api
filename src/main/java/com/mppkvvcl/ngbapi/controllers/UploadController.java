package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.UploadService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.utility.NGBAPIUtility;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@RestController
@RequestMapping(value = "upload")
public class UploadController {


    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(UploadController.class);

    @Autowired
    private UploadService uploadService;

    @RequestMapping(method = RequestMethod.POST,value = "/read", produces = "application/json")
    public ResponseEntity<?> fileUpload(@RequestParam("uploadFile") MultipartFile file) {
        String methodName = "fileUpload (): ";
        logger.info(methodName + "called");
        ErrorMessage errorMessage = null;
        ResponseEntity<?> response = null;
        Workbook workbook = null;
        if (file == null || file.isEmpty() || file.getOriginalFilename() == null) {
            logger.error(methodName + "inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            return response;
        }
        logger.info(methodName + "file name " + file.getOriginalFilename());
        if (!file.getOriginalFilename().endsWith(".xls") && !file.getOriginalFilename().endsWith(".xlsx")) {
            logger.error(methodName + "given file is not valid.");
            errorMessage = new ErrorMessage("given file is not valid.");
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            return response;
        }
        try {
            errorMessage = new ErrorMessage();
            workbook = NGBAPIUtility.getWorkBookFromFile(file);
            workbook = uploadService.uploadReadMaster(workbook, errorMessage);
            if (workbook != null) {
                logger.info(methodName + "writing workbook");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                workbook.write(byteArrayOutputStream);
                response = new ResponseEntity<>(byteArrayOutputStream.toByteArray(), HttpStatus.OK);
            } else {
                logger.error(methodName + "some error in uploading reading data ");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        } catch (RuntimeException exception) {
            errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + exception.getMessage());
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        } catch (Exception exception) {
            errorMessage = new ErrorMessage(exception.getMessage());
            logger.error(methodName + "Received Exception in controller with error message as :" + exception.getMessage());
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        } finally {
            try {
                if (workbook != null) {
                    logger.info(methodName + "closing workbook");
                    workbook.close();
                }
            } catch (IOException exception) {
                logger.error(methodName + "Received I/O Exception in controller with error message as :" + exception.getMessage());
            }
        }
        return response;
    }
}
