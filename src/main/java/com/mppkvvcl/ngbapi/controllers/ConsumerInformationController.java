package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.ConsumerInformationService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerInformation;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by SUMIT on 08-06-2017.
 */
@RestController
@RequestMapping(value = "/consumer-information")
public class ConsumerInformationController {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerInformationController.class);

    /**
     * Asking spring to inject ConsumerInformationService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on consumer_information table data at the backend database.
     */
    @Autowired
    private ConsumerInformationService consumerInformationService;

    /**
     * URI consumer-information/consumer-no/{consumerNo}<br><br>
     * This method at controller with URI
     * provides ConsumerInformation details with passed consumerNo parameter.<br><br>
     * Response 200 for OK status and consumerInformation object of ConsumerInformation is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content found.<br>
     * Response 404 for NOT_FOUND status is sent when consumerInformation object is null.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null.<br><br>
     * param consumerNo<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET,value = "/consumer-no/{consumerNo}",produces = "application/json")
    public ResponseEntity getConsumerInformationByConsumerNo(@PathVariable("consumerNo") String consumerNo)  {
        String methodName = "getConsumerInformationByConsumerNo() : ";
        ConsumerInformationInterface consumerInformation = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "Got request to get ConsumerInformation by consumerNo : " + consumerNo);
        if(consumerNo != null){
            logger.info(methodName + "Calling ConsumerInformationService method to get ConsumerInformation");
            try{
                consumerInformation = consumerInformationService.getConsumerInformationByConsumerNo(consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                consumerInformation = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                consumerInformation  = null;
            }
            if(consumerInformation != null){
                logger.info(methodName+"Got ConsumerInformation as : "+consumerInformation);
                response = new ResponseEntity<>(consumerInformation,HttpStatus.OK);
            }else{
                logger.error(methodName+"Got ConsumerInformation as null");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"Got input consumerNo as null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - consumer-information/consumer-name/{consumerName}<br><br>
     * This method at controller with URI provides ConsumerInformation details with passed consumerName parameter<br><br>
     * Response 200 for OK status and list of consumersInformation is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 417 for EXPECTATION_FAILED status is sent if list is empty.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null.<br><br>
     * param consumerName<br><br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/consumer-name/{consumerName}",produces = "application/json")
    public ResponseEntity getConsumerInformationByConsumerName(@PathVariable("consumerName") String consumerName){
        String methodName = " getConsumerInformationByConsumerName :  ";
        List<ConsumerInformationInterface> consumersInformation = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to get ConsumersInformation by consumerName : " + consumerName);
        if(consumerName != null){
            logger.info(methodName + "Calling ConsumersInformationService method to get ConsumersInformation");
            consumersInformation = consumerInformationService.getConsumersInformationByConsumerName(consumerName);
            if(consumersInformation != null){
                if(consumersInformation.size()>0){
                    logger.info(methodName + "Got ConsumersInformation as : " + consumersInformation);
                    response = new ResponseEntity<>(consumersInformation,HttpStatus.OK);
                }else{
                    logger.info(methodName+"Got ConsumersInformation as : " + consumersInformation);
                    response = new ResponseEntity<>(consumersInformation,HttpStatus.NO_CONTENT);
                }
             }else{
                logger.error(methodName + "Got ConsumersInformation as null");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"Got input consumerName as null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }


    /**
     * URI consumer-information/consumer-no/{consumerNo}<br><br>
     * This method at controller with URI
     * change ConsumerInformation details with passed consumerInformation Bean.<br><br>
     * Response 200 for OK status and consumerInformation object of ConsumerInformation is sent if successful.<br>
     * Response 417 for EXPECTATION_FAILED status is sent when updation failed<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null.<br><br>
     * param consumerNo<br>
     * return response
     */
    /*@RequestMapping(method = RequestMethod.PUT,produces = "application/json", consumes = "application/json")
    public ResponseEntity update(@RequestBody ConsumerInformation consumerInformation) throws Exception {
        String methodName = " update() :  ";
        ConsumerInformationInterface updatedConsumerInformation = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "called, by consumer : "+consumerInformation);
        if( consumerInformation != null){
            logger.info(methodName + "Calling ConsumerInformationService");
            try{
                updatedConsumerInformation = consumerInformationService.updateConsumerInformation(consumerInformation);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                updatedConsumerInformation = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                updatedConsumerInformation  = null;
            }
            if(updatedConsumerInformation != null){
                logger.info(methodName+"updated ConsumerInformation as : "+updatedConsumerInformation);
                response = new ResponseEntity<>(updatedConsumerInformation,HttpStatus.OK);
            }else{
                logger.error(methodName+"updating ConsumerInformation failed");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"Got input consumerNo as null");
            errorMessage = new ErrorMessage("Got input consumerNo as null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }*/

    @RequestMapping(method = RequestMethod.PUT,value = "/consumer-no/{consumerNo}",produces = "application/json", consumes = "application/json")
    public ResponseEntity update(@RequestBody Map<String,String> consumerInformationMap,@PathVariable("consumerNo") String consumerNo) throws Exception {
        String methodName = "update() : ";
        logger.info(methodName + "called for " + consumerNo + " with map " + consumerInformationMap);
        ConsumerInformationInterface updatedConsumerInformation = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if(consumerNo != null && consumerInformationMap != null && consumerInformationMap.size() > 0){
            try{
                updatedConsumerInformation = consumerInformationService.updateConsumerInformation(consumerNo,consumerInformationMap);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                updatedConsumerInformation = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                updatedConsumerInformation  = null;
            }
            if(updatedConsumerInformation != null){
                logger.info(methodName+"updated ConsumerInformation as : "+updatedConsumerInformation);
                response = new ResponseEntity<>(updatedConsumerInformation,HttpStatus.OK);
            }else{
                logger.error(methodName+"updating ConsumerInformation failed");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"Got input consumerNo as null");
            errorMessage = new ErrorMessage("Got input consumerNo as null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/consumer-no/{consumerNo}/bpl",produces = "application/json", consumes = "application/json")
    public ResponseEntity updateBPL(@RequestBody Map<String,String> consumerInformationMap,@PathVariable("consumerNo") String consumerNo)  {
        String methodName = "updateBPL() : ";
        logger.info(methodName + "called for " + consumerNo + " with map " + consumerInformationMap);
        ConsumerInformationInterface updatedConsumerInformation = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if(consumerNo != null && consumerInformationMap != null && consumerInformationMap.size() > 0){
            try{
                updatedConsumerInformation = consumerInformationService.updateBPLConsumerInformation(consumerNo,consumerInformationMap);
            }catch(RuntimeException exception){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + exception.getMessage());
                updatedConsumerInformation = null;
            }catch (Exception exception){
                errorMessage = new ErrorMessage(exception.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + exception.getMessage());
                updatedConsumerInformation  = null;
            }
            if(updatedConsumerInformation != null){
                logger.info(methodName+"updated ConsumerInformation as : "+updatedConsumerInformation);
                response = new ResponseEntity<>(updatedConsumerInformation,HttpStatus.OK);
            }else{
                logger.error(methodName+"updating ConsumerInformation failed");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"Got input consumerNo as null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/consumer-no/{consumerNo}/employee",produces = "application/json", consumes = "application/json")
    public ResponseEntity updateEmployee(@RequestBody Map<String,String> consumerInformationMap,@PathVariable("consumerNo") String consumerNo)  {
        String methodName = "updateEmployee() : ";
        logger.info(methodName + "called for " + consumerNo + " with map " + consumerInformationMap);
        ConsumerInformationInterface updatedConsumerInformation = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if(consumerNo != null && consumerInformationMap != null && consumerInformationMap.size() > 0){
            try{
                updatedConsumerInformation = consumerInformationService.updateEmployeeConsumerInformation(consumerNo,consumerInformationMap);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                updatedConsumerInformation = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                updatedConsumerInformation  = null;
            }
            if(updatedConsumerInformation != null){
                logger.info(methodName+"updated ConsumerInformation as : "+updatedConsumerInformation);
                response = new ResponseEntity<>(updatedConsumerInformation,HttpStatus.OK);
            }else{
                logger.error(methodName+"updating ConsumerInformation failed");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"Got input consumerNo as null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
