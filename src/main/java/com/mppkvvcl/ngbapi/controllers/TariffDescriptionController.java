package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbinterface.interfaces.TariffDescriptionInterface;
import com.mppkvvcl.ngbapi.services.TariffDescriptionService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by SUMIT on 19-05-2017.
 *
 *   This tariff controller defines various uri for tariffDescription resource under nsc database
 * and nsc schema for nextgenbillingbackend.This REST API for tariffs has the required and related
 * methods for fulfilling front end request
 *
 *  author  Sumit Verma
 * version 1.0
 * last change @since   19-05-2017
 **/

@RestController
@RequestMapping(value="/tariff-description")
public class TariffDescriptionController {

    private static final Logger logger = GlobalResources.getLogger(TariffDescriptionController.class);

    @Autowired
    private TariffDescriptionService tariffDescriptionService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAllTariffDescription(){
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        List<? extends TariffDescriptionInterface> tariffDescriptions = tariffDescriptionService.getAll();
        if(tariffDescriptions != null){
            response = new ResponseEntity<>(tariffDescriptions, HttpStatus.OK);
        }
        return response;
    }
}
