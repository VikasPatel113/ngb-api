package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionAreaInformationInterface;
import com.mppkvvcl.ngbdao.daos.ConsumerConnectionAreaInformationDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by PREETESH on 8/28/2017.
 */
@RestController
@RequestMapping(value = "consumer-connection-area-information")
public class ConsumerConnectionAreaInformationController {

    private static final Logger logger = GlobalResources.getLogger(ConsumerConnectionAreaInformationController.class);


    @Autowired
    private ConsumerConnectionAreaInformationDAO consumerConnectionAreaInformationService;


    @RequestMapping(value = "/consumer-no/{consumerNo}",method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getByConsumerNo(@PathVariable("consumerNo") String consumerNo){
        String methodName = "getByConsumerNo()";
        ResponseEntity response = null;
        ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation;
        logger.info(methodName + " Got request to fetch Consumer connection information details with consumer no : " + consumerNo);
        if (consumerNo != null){
            logger.info(methodName + "Calling Consumer Connection Service to fetch details");
            consumerConnectionAreaInformation = consumerConnectionAreaInformationService.getByConsumerNo(consumerNo);
            if (consumerConnectionAreaInformation != null){
                logger.info(methodName + "Fetched consumer connection information successfully. ");
                response = new ResponseEntity(consumerConnectionAreaInformation, HttpStatus.OK);
            }else {
                logger.error(methodName + "Couldn't retrieve consumer connection information due some error");
                response = new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + " Got Consumer no is null.");
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
