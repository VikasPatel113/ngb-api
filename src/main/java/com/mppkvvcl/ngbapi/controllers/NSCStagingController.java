package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.NSCStagingService;
import com.mppkvvcl.ngbapi.services.NSCStagingStatusService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingInterface;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.NSCStaging;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by SUMIT on 31-05-2017.
 */
@RestController
@RequestMapping(value = "/nsc")
public class NSCStagingController {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(NSCStagingController.class);

    /**
     * Asking spring to inject NSCStagingService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on nsc_staging table data at the backend database.
     */
    @Autowired
    private NSCStagingService nscStagingService;

    @Autowired
    private NSCStagingStatusService nscStagingStatusService;

    /**
     * URI - /nsc<br><br>
     * This method provides to add New Service Connection API with URL /nsc.
     * When above mentioned API is hit the nscStaging data is inserted to respective nsc_staging table.<br><br>
     * Response 200 for OK with various nsc data along with application id is sent if successful.<br>
     * Response 417 for EXPECTATION_FAILED is sent if insertion fails.<br>
     * Response 400 for BAD_REQUEST is sent if input param found null.<br><br>
     * param nscStaging<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public ResponseEntity postNSCStaging(@RequestBody NSCStaging nscStaging){
        String methodName="postNSCStaging ";
        logger.info(methodName +": Got New Service Connection Staging data as "+nscStaging+"  for saving data of NSC.");
        ResponseEntity<?> response = null;
        if(nscStaging != null){
            logger.info(methodName+": Saving submitted data for NSC Staging");
            NSCStagingInterface insertedNSCStaging = nscStagingService.insert(nscStaging);
            if(insertedNSCStaging != null){
                logger.info(methodName+": NSCStaging data saved with staging id as : "+insertedNSCStaging.getId());
                response = new ResponseEntity<>(insertedNSCStaging,HttpStatus.CREATED);
            }else{
                logger.error(methodName+": NSCStaging error . nscStagingServiceData may be NULL ");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+": nscStaging is NULL");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
