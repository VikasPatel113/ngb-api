package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomPayment;
import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.InstrumentDetailService;
import com.mppkvvcl.ngbapi.services.PaymentService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.Payment;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * Created by ANSHIKA on 15-07-2017.
 */
@RestController
@RequestMapping(value = "/payment")
public class PaymentController {

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(PaymentController.class);

    /**
     * Requesting spring to get singleton PaymentService object
     */
    @Autowired
    private PaymentService paymentService;

    @Autowired
    private InstrumentDetailService instrumentDetailService;

    /**
     * code by nitish
     * Below method returns count of readings for consumerNo
     * @param consumerNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getCountByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            long count = paymentService.getCountByConsumerNo(consumerNo);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/paged",  produces = "application/json" )
    public ResponseEntity<?> getByConsumerNoWithPagination(@PathVariable("consumerNo") String consumerNo,
                                                           @PathParam("sortBy") String sortBy,@PathParam("sortOrder") String sortOrder,
                                                           @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize){
        String methodName = "getByConsumerNoWithPagination() : ";
        logger.info(methodName + "called " + consumerNo + " pageNumber " + pageNumber + " pageSize " + pageSize + " sort " + sortOrder + " sortBy " + sortBy);
        ResponseEntity<?> response = null;
        List<PaymentInterface> paymentInterfaces = null;
        if(consumerNo != null && sortBy != null){
            paymentInterfaces = paymentService.getByConsumerNoWithPagination(consumerNo,sortBy,sortOrder,pageNumber,pageSize);
            if(paymentInterfaces != null){
                response = new ResponseEntity<>(paymentInterfaces, HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Input param consumerNo :" + consumerNo + "found null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /payment/consumer-no/{consumerNo}<br>
     * This method takes consumerNo and fetch a list of payments.<br>
     * Response 200 for OK status and list of payments is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br>
     * param consumerNo<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}",  produces = "application/json" )
    public ResponseEntity<?> getByConsumerNo(@PathVariable("consumerNo") String consumerNo){
        String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called for consumerNo : " + consumerNo);
        ResponseEntity<?> response = null;
        List<PaymentInterface> payments = null;
        if(consumerNo != null){
            logger.info(methodName + "Calling PaymentService method to fetch list of payments");
            payments = paymentService.getByConsumerNo(consumerNo);
            if(payments != null){
                if(payments.size() >0){
                    logger.info(methodName + "Got payments against consumerNo : " + consumerNo );
                    response = new ResponseEntity<>(payments, HttpStatus.OK);
                }else{
                    logger.error(methodName + "No content found in list");
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error(methodName + "Payment list not found against consumerNo : " + consumerNo);
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName + "Input param consumerNo :" + consumerNo + "found null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /payment/location-code/{locationCode}/posting-bill-month/{postingBillMonth}<br><br>
     * This method takes locationCode and postingBillMonth and fetch a list of payments.<br><br>
     * Response 200 for OK status and list of payments is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br><br>
     * param locationCode<br>
     * param postingBillMonth<br>
     * return response
     */
    @RequestMapping( method = RequestMethod.GET, value = "/location-code/{locationCode}/posting-bill-month/{postingBillMonth}", produces = "application/json")
    public ResponseEntity<?> getByLocationCodeAndPostingBillMonth(@PathVariable("locationCode") String locationCode, @PathVariable("postingBillMonth") String postingBillMonth){
        String methodName = "getByLocationCodeAndPostingBillMonth() : ";
        logger.info(methodName + "called for locationCode : " + locationCode + "and postingBillMonth : " + postingBillMonth);
        ResponseEntity<?> response = null;
        List<PaymentInterface> payments = null;
        if(locationCode != null && postingBillMonth != null){
            logger.info(methodName + "Calling PaymentService method to fetch list of payments");
            payments = paymentService.getByLocationCodeAndPostingBillMonth(locationCode, postingBillMonth);
            if(payments != null){
                if(payments.size() > 0){
                    logger.info(methodName + "Got PaymentList against locationCode : " + locationCode + "and postingBillMonth : " + postingBillMonth);
                    response = new ResponseEntity<>(payments, HttpStatus.OK);
                }else{
                    logger.error(methodName + "No content found in list");
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error(methodName + "Payements list not found against locationCode : " + locationCode + "and postingBillMonth : " + postingBillMonth);
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName + "Input param locationCode : " + locationCode + "and postingBillMonth : " + postingBillMonth + "found null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return  response;
    }

    /**
     *
     * @param payment
     * URI - /payment/id/{id}<br>
     * This method takes payment and update the existing one.<br>
     * Response 200 for OK status if payments updated successful.<br>
     * Response 417 for EXPECTATION_FAILED status is sent when unable to update payment.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br>
     * param consumerNo<br>
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json" )
    public ResponseEntity<?> update(@RequestBody Payment payment){
        String methodName = "update() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        PaymentInterface updatedPayment = null;
        if(payment != null) {
            try {
                updatedPayment = paymentService.updatePayment(payment);
            }
            catch (RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "RuntimeException : " + e.getMessage());
                updatedPayment=null;
            }
            catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Exception : " + ee.getMessage());
                updatedPayment = null;
            }
            if (updatedPayment != null) {
                logger.info(methodName + "payment updated Successfully");
                response = new ResponseEntity<>(updatedPayment, HttpStatus.OK);
            } else {
                logger.error(methodName + "some error in updating Payment");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Payment is  null");
            errorMessage = new ErrorMessage("Payment is null");
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }


    @RequestMapping( method = RequestMethod.DELETE, value = "/id/{id}", produces = "application/json" )
    public ResponseEntity<?> deletePaymentById(@PathVariable("id") long id){
        String methodName = "deletePaymentById() : ";
        logger.info(methodName + "called for id : " + id);
        ResponseEntity<?> response = null;
        PaymentInterface paymentDeleted = null;
        ErrorMessage errorMessage = null;

        logger.info(methodName + "Got request to delete payments against id : " + id);

        logger.info(methodName + "Calling PaymentService method to delete Payment by id");
        try {
            paymentDeleted = paymentService.deletePaymentById(id);
        }
        catch (RuntimeException e){
            errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
        }
        catch (Exception ee){
            errorMessage = new ErrorMessage(ee.getMessage());
            logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());

        }

        if(paymentDeleted != null) {
            logger.info(methodName + " payment deleted Successfully  : " + id);
            response = new ResponseEntity<>(paymentDeleted,HttpStatus.OK);
        }else{
            logger.error(methodName + " unable to delete  : " + id);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }

        return response;
    }

    /**
     * Added By: Preetesh Date: 13 Sep 2017
     * URI for posting payments through cheque
     */
    @RequestMapping(method = RequestMethod.POST, value = "multiPaymentByInstrument/bank-name/{bankName}/pay-mode/{payMode}/instrument-no/{instrumentNo}/instrument-date/{instrumentDateString}/micr-code/{micrCode}/amount/{amount}", produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> postMultiplePaymentsThroughInstrument(
            @PathVariable String bankName,
            @PathVariable String payMode,
            @PathVariable String instrumentNo,
            @PathVariable  String instrumentDateString,
            @PathVariable  String micrCode,
            @PathVariable long amount,
            @RequestBody List<Payment> payments
    ) {
        String methodName = "postMultiplePaymentsThroughInstrument() : ";
        ResponseEntity<?> response = null;
        List<PaymentInterface> paymentPosted = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "Got request to post multiple payments through single instrument, inputs are BankName: "
                +bankName+" payMode: "+payMode+" instrumentNo: "+instrumentNo+" instrumentDateString: "+instrumentDateString+" micrCode "+micrCode+" amount "+amount);
        if (bankName != null && payMode != null && instrumentNo != null && instrumentDateString != null && micrCode != null
                && payments != null && payments.size()>0 ) {
            try{
                paymentPosted = paymentService.insertMultiplePaymentsByInstrument(bankName,payMode, instrumentNo, instrumentDateString, micrCode,amount,payments);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                paymentPosted = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                paymentPosted = null;
            }

            if (paymentPosted != null && paymentPosted.size() == payments.size()) {
                logger.info(methodName + "payments saved successfully ");
                response = new ResponseEntity<>(paymentPosted,HttpStatus.OK);
            } else {
                logger.error(methodName + "some error in saving payment");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "some input is null");
            errorMessage = new ErrorMessage("some input is null");
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);

        }
        return response;
    }


    /**
     * Added By: Preetesh Date: 11 Aug 2017
     * URI for posting payment
     * @param customPayment
     * @return
     */
    @RequestMapping(method = RequestMethod.POST , value = "/custom", produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> insertCustomPayment(@RequestBody CustomPayment customPayment) {
        String methodName = "insertCustomPayment() : ";
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        PaymentInterface payment = null;
        if(customPayment != null){
            try{
                payment = paymentService.insertCustomPayment(customPayment);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                payment = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                payment = null;
            }

            if (payment != null) {
                logger.info(methodName + "payment saved successfully, payment: "+payment);
                response = new ResponseEntity<>(payment, HttpStatus.OK);
            } else {
                logger.error(methodName + "some error in saving payment");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "some input is null");
            errorMessage = new ErrorMessage("some input is null");
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
