package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.GovernmentTypeService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.GovernmentTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * Created by ANSHIKA on 18-07-2017.
 */
@RestController
@RequestMapping(value = "/government-type")
public class GovernmentTypeController {
    /**
     * Requesting spring to get singleton GovernmentTypeController object.
     */
    @Autowired
    GovernmentTypeService governmentTypeService;

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(GovernmentTypeController.class);

    /**
     * URI - /government-type<br><br>
     * This method fetches the list of govenment types from the backend database when user hit the URI.<br><br>
     * Response 200 for OK status and list of government types is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() : ";
        List<? extends GovernmentTypeInterface> governmentTypes = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to view list of government type");
        governmentTypes = governmentTypeService.getAll();
        if(governmentTypes != null){
            if(governmentTypes.size() > 0){
                logger.info(methodName + "List of government types received successfully with rows : " + governmentTypes.size());
                response = new ResponseEntity<>(governmentTypes, HttpStatus.OK);
            }else{
                logger.error(methodName + "No content found in list");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else{
            logger.error(methodName + "List of government types not found");
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
