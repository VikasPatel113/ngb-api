package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.BillCalculationLineService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.BillCalculationLineInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "bill/calculation/line")
public class BillCalculationLineController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(BillCalculationLineController.class);

    /*
    Asking spring to inject BillService object so that we can use
    its methods for performing various operations through repo.
     */
    @Autowired
    private BillCalculationLineService billCalculationLineService;

    @RequestMapping(method = RequestMethod.GET, value = "/bill-id/{billId}",  produces = "application/json" )
    public ResponseEntity<?> getByBillId(@PathVariable("billId") long billId){
        String methodName = "getByBillId() : ";
        logger.info(methodName + "called ");
        ResponseEntity<?> response = null;
        List<BillCalculationLineInterface> billCalculationLineInterfaces = billCalculationLineService.getByBillId(billId);
        if(billCalculationLineInterfaces != null){
            response = new ResponseEntity<>(billCalculationLineInterfaces, HttpStatus.OK);
        }else{
            response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
