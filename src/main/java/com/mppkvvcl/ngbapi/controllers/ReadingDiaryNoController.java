package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.ReadingDiaryNoService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.ReadingDiaryNo;
import com.mppkvvcl.ngbinterface.interfaces.ReadingDiaryNoInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "reading/diary")
public class ReadingDiaryNoController {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ReadingDiaryNoController.class);

    /**
     * Asking spring to inject ReadingDiaryNoService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on ReadingDiaryNo table data at the backend database.
     */
    @Autowired
    private ReadingDiaryNoService readingDiaryNoService;

    @RequestMapping(method = RequestMethod.GET,value = "group/no/{groupNo}")
    public ResponseEntity getByGroupNo(@PathVariable("groupNo") String groupNo){
        String methodName = "getByGroupNo() : ";
        List<ReadingDiaryNoInterface> readingDiaryNos = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "called ");
        if(groupNo != null){
            logger.info(methodName + "calling service to get reading diaries with group no as :" + groupNo);
            readingDiaryNos = readingDiaryNoService.getByGroupNo(groupNo);
            if(readingDiaryNos != null && readingDiaryNos.size() >0){
                logger.info(methodName + "fetched reading diaries successfully with size as : " + readingDiaryNos.size());
                response = new ResponseEntity<>(readingDiaryNos, HttpStatus.OK);
            }else {
                errorMessage = new ErrorMessage("no reading diary found for group No "+groupNo);
                response = new ResponseEntity<>(errorMessage,HttpStatus.NO_CONTENT);
            }
        }else{
            logger.error(methodName + "got group no as null. sending bad request");
            errorMessage = new ErrorMessage("got group no as null");
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }


    /**
     * URI - /reading/diary<br><br>
     * This create method takes ReadingDiaryNo object and insert it into the backend database when user hit the URI.<br><br>
     * Response 201 for CREATED and reading diary object is sent if successful<br>
     * Response 417 for EXPECTATION_FAILED is sent when insertion fail<br>
     * Response 400 for BAD_REQUEST is sent when input param found null.<br><br>
     * param group<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public ResponseEntity create(@RequestBody ReadingDiaryNo readingDiaryNoToCreate){
        String methodName = "create() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        ReadingDiaryNoInterface createdReadingDiaryNo = null;
        ErrorMessage errorMessage = new ErrorMessage();
        if(readingDiaryNoToCreate != null ){
            createdReadingDiaryNo = readingDiaryNoService.create(readingDiaryNoToCreate,errorMessage);
            if(createdReadingDiaryNo != null){
                logger.info(methodName + "reading diary created successfully");
                response = new ResponseEntity<>(createdReadingDiaryNo,HttpStatus.CREATED);
            }else{
                logger.error(methodName + "error in creating reading diary");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "ReadingDiary object is NULL");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
